package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import gas.iorm.core.api.service.IORMClassProcessor;
import gas.iorm.core.api.service.IORMDataModel;
import gas.iorm.core.storage.*;
import org.apache.ignite.*;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMemoryMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.lang.IgniteBiPredicate;
import org.apache.ignite.lang.IgniteCallable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by alex on 10.06.16.
 */
public class IORMFactory {

    private static final Logger logger = LoggerFactory.getLogger(IORMFactory.class);

    private Ignite ignite;
    private List<Class<? extends IORMPersistable>> classes = new LinkedList<Class<? extends IORMPersistable>>();
    private IgniteTransactions txManager;
    private final Map<String, IORMCacheStorage> storageHolder = new HashMap<String, IORMCacheStorage>();
    private final IORMClassProcessor classProcessor;
    private IORMDataModel dataModel;
    private final IORMFactoryConfiguration configuration;

    private IgniteAtomicLong numerator;

    private static IORMFactory instance = null;
    private static final Lock factoryLock = new ReentrantLock();

    private IgniteCache<Long, Long> partitionedMap;

    public static IORMFactory getInstance() {
        if (IORMFactory.instance == null) {
            throw new RuntimeException("IORMFactory not initialized. To initialize invoke " +
                    "IORMFactory.getInstance(IORMFactoryConfiguration configuration)");
        }
        return IORMFactory.instance;
    }

    public static IORMFactory getInstance(IORMFactoryConfiguration configuration) {
        if (IORMFactory.instance != null) {
            logger.warn("Instance of IROMFactory already initialized with configuration {}", instance.configuration);
            return IORMFactory.instance;
        }
        try {
            if (factoryLock.tryLock(60, TimeUnit.SECONDS)) {
                if (IORMFactory.instance == null) {
                    IORMFactory.instance = new IORMFactory(configuration);
                    IORMFactory.instance.initialize();
                }
                factoryLock.unlock();
            }
        } catch (ClassNotFoundException ne) {
            factoryLock.unlock();
            throw new RuntimeException("Error processing classes.", ne);
        } catch (InterruptedException ie) {
            throw new RuntimeException("Error initialize instance IORMFactory.", ie);
        }
        return IORMFactory.instance;
    }

    private IORMFactory(IORMFactoryConfiguration configuration) {
        this.configuration = configuration;
        this.classProcessor = new IORMClassProcessor(configuration.getClassPaths());
    }

    private void initialize() throws ClassNotFoundException {
        addClasses(configuration.getClassNames());
        init();
    }

    @SuppressWarnings("unchecked")
    private void addClasses(String... classNames) throws ClassNotFoundException {
        try {
            List<Class<?>> clsList = classProcessor.processClasses(classNames);
            dataModel = new IORMDataModel(clsList);
            for (Class<?> clazz : clsList) {
                final Field factoryField = clazz.getDeclaredField("___factory");
                factoryField.set(null, this);
                this.classes.add((Class<? extends IORMPersistable>) clazz);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ClassCastException("Error at preparing classes");
        }
    }

    private void initStorages() {
        for (Class<? extends IORMPersistable> clazz : this.classes) {
            getStorage(clazz);
        }
    }

//    private boolean implementIORMPersistable(Class<?> clazz) {
//        for (Class<?> i : clazz.getInterfaces()) {
//            if (i.equals(IORMPersistable.class)) {
//                return true;
//            }
//        }
//        return false;
//    }

    private void initPartitionedMap() {
        final CacheConfiguration<Long, Long> config = new CacheConfiguration<Long, Long>();
        config.setName("___Partitioned___Map_")
                .setCacheMode(CacheMode.REPLICATED)
                .setBackups(0);
        this.partitionedMap = ignite.getOrCreateCache(config);
    }

    private void init() {
        if (this.configuration.getDefaultIgniteConfigurationURL() != null) {
            ignite = Ignition.start(this.configuration.getDefaultIgniteConfigurationURL());
        } else {
            final IgniteConfiguration config = new IgniteConfiguration();
            config.setCacheConfiguration(getCommonCacheConfiguration());
            ignite = Ignition.start(config);
        }
        this.txManager = this.ignite.transactions();

        this.numerator = ignite.atomicLong("IORMEntityKeyGenerator", Long.MIN_VALUE + 1, true);
        initPartitionedMap();
        initStorages();

        final IgniteCallable<String> callable = new IgniteCallable<String>() {
            public String call() throws Exception {
                System.out.println("IORM Initialized )))");
                return "IORM Initialized";
            }
        };
        ignite.compute().call(callable);
    }

    public void stop() {
        Ignition.stop(false);
    }

    public IORMStorage getStorage() {
        return new IORMStorageImpl(this, this.txManager);
    }

    private boolean implementInterface(Class<?> clazz, Class<?> interf) {
        for (Class<?> c : clazz.getInterfaces()) {
            if (c.equals(interf)) {
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public<T extends IORMPersistable> IORMCacheStorage<T> getStorage(Class<T> clazz) {
        if (!this.storageHolder.containsKey(clazz.getCanonicalName())) {
            if (implementInterface(clazz, IORMPartitioned.class)) {
                final CacheConfiguration<IORMPartitionKey, T> config = getPartitionedCacheConfiguration((Class)clazz);
                final IgniteCache<IORMPartitionKey, T> cache = ignite.<IORMPartitionKey, T>getOrCreateCache(config);
                this.storageHolder.put(clazz.getCanonicalName(), new IORMCacheStoragePartitioned<>((IgniteCache)cache, (Class)clazz,
                        numerator, txManager, partitionedMap));

                if (config.getCacheStoreFactory() != null) {
                    final PartitionedCacheNumeratorLoader loader = new PartitionedCacheNumeratorLoader();
                    cache.loadCache(loader);
                    long lastKey = loader.getMaxValue() + 1;
                    long currentKey = numerator.get();
                    if (lastKey > currentKey) {
                        numerator.compareAndSet(currentKey, lastKey);
                    }
                }
            } else {
                final CacheConfiguration<Long, T> config = getCacheConfiguration(clazz);
                final IgniteCache<Long, T> cache = ignite.<Long, T>getOrCreateCache(config);
                this.storageHolder.put(clazz.getCanonicalName(), new IORMCacheStoragePersistable<T>(cache, clazz,
                        numerator, txManager));

                if (config.getCacheStoreFactory() != null) {
                    final CacheNumeratorLoader<T> loader = new CacheNumeratorLoader<>();
                    cache.loadCache(loader);
                    long lastKey = loader.getMaxValue() + 1;
                    long currentKey = numerator.get();
                    if (lastKey > currentKey) {
                        numerator.compareAndSet(currentKey, lastKey);
                    }
                }
            }

        }
        return this.storageHolder.get(clazz.getCanonicalName());
    }

    private <T extends IORMPersistable> long maxCacheKey(IgniteCache<Long, T> cache) {
        long lastKey = Long.MIN_VALUE + 1 + cache.sizeLong();
        while (cache.containsKey(lastKey)) {
            lastKey++;
        }
        return lastKey;
    }

    private CacheConfiguration getCommonCacheConfiguration() {
        final CacheConfiguration config = new CacheConfiguration();
        config.setBackups(0);
        config.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        config.setMemoryMode(CacheMemoryMode.ONHEAP_TIERED);
        config.setOffHeapMaxMemory(0L);
//        config.setStartSize(1024 * 16);
        return config;
    }

    private<T extends IORMPersistable> CacheConfiguration<Long, T> getCacheConfiguration(Class<T> clazz) {
        final CacheConfiguration<Long, T> config = new CacheConfiguration<Long, T>();
        final IORMCacheConfiguration<T> configBuilder = new IORMCacheConfiguration<T>();
        config.setName(clazz.getCanonicalName());
        config.setIndexedTypes(Long.class, clazz);
        config.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        configBuilder.applyAnnotationConfiguration(clazz, config);
        return config;
    }

    private <T extends IORMPartitioned> CacheConfiguration<IORMPartitionKey<T>, T> getPartitionedCacheConfiguration(Class<T> clazz) {
        final CacheConfiguration<IORMPartitionKey<T>, T> config = new CacheConfiguration<IORMPartitionKey<T>, T>();
        final IORMPartitionedCacheConfiguration<T> configBuilder = new IORMPartitionedCacheConfiguration<T>();
        config.setName(clazz.getCanonicalName());
        config.setIndexedTypes(IORMPartitionKey.class, clazz);
        config.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        configBuilder.applyAnnotationConfiguration(clazz, config);
        return config;
    }

    public Class<?> getOriginalFieldType(Class<?> clazz, String fieldName) {
        Class<?> result = classProcessor.getFieldType(clazz, fieldName);
        if (result == null) {
            result = dataModel.getFieldType(clazz, fieldName);
        }
        return  result;
    }

    public boolean containClass(Class<?> clazz) {
        return dataModel.containClass(clazz);
    }

    public Class<?> getFieldType(Class<?> clazz, String fieldName) {
        return dataModel.getFieldType(clazz, fieldName);
    }

    public Collection<ClusterNode> getNodeCollection() {
        return ignite.cluster().nodes();
    }

    public ClusterGroup groupNodes(ClusterNode node, ClusterNode... nodes) {
        return ignite.cluster().forNode(node, nodes);
    }

    private class CacheNumeratorLoader<T extends IORMPersistable> implements IgniteBiPredicate<Long, T> {
        private long maxValue = Long.MIN_VALUE;
        @Override
        public boolean apply(Long key, T t) {
            if (maxValue < key) {
                maxValue = key;
            }
            return true;
        }

        public long getMaxValue() {
            return maxValue;
        }
    }

    private class PartitionedCacheNumeratorLoader<T extends IORMPartitioned> implements IgniteBiPredicate<IORMPartitionKey<T>, T> {
        private long maxValue = Long.MIN_VALUE;
        @Override
        public boolean apply(IORMPartitionKey<T> key, T t) {
            if (maxValue < key.getId()) {
                maxValue = key.getId();
            }
            return true;
        }

        public long getMaxValue() {
            return maxValue;
        }
    }
}
