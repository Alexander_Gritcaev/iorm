package gas.iorm.core.api;

/**
 * Created by alex on 21.06.16.
 */
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}
