package gas.iorm.core.api.service;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Created by alex on 28.06.16.
 */
public class IORMDataModel {

    private final Map<Class<?>, Map<String, Class<?>>> dataModel =
            new HashMap<Class<?>, Map<String, Class<?>>>();

    public IORMDataModel(List<Class<?>> classes) {
        for (Class<?> clazz : classes) {
            processClass(clazz);
        }
    }

    private void processClass(Class<?> clazz) {
        final Map<String, Class<?>> map = new HashMap<String, Class<?>>();

        final List<Field> fields = new LinkedList<Field>();
        getDeclaredFields(clazz, fields);
        for (Field field : fields) {
            if (!Modifier.isTransient(field.getModifiers()) && !isCollection(field)) {
                map.put(field.getName(), field.getType());
            }
        }

        dataModel.put(clazz, map);
    }

    private boolean isCollection(Field field) {
        for (Class<?> inter : field.getType().getInterfaces()) {
            if (inter.equals(Collection.class)) {
                return  true;
            }
        }
        return false;
    }


    private void getDeclaredFields(Class<?> clazz, List<Field> resultList) {
        resultList.addAll(Arrays.asList(clazz.getDeclaredFields()));
        if (!clazz.getSuperclass().equals(Object.class)) {
            getDeclaredFields(clazz.getSuperclass(), resultList);
        }
    }

    public Class<?> getFieldType(Class<?> clazz, String fieldName) {
        if (dataModel.containsKey(clazz) && dataModel.get(clazz).containsKey(fieldName)) {
            return dataModel.get(clazz).get(fieldName);
        } else {
            return null;
        }
    }

    public boolean containClass(Class<?> clazz) {
        return dataModel.containsKey(clazz);
    }

}
