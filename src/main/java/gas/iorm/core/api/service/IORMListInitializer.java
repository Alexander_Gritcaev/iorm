package gas.iorm.core.api.service;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;

import java.util.*;

/**
 * Created by alex on 20.06.16.
 */
public class IORMListInitializer {

    public static <T extends IORMPersistable> List<T> initializeListFromId(Class<T> clazz, long[] ids, IORMFactory factory) {
        final Set<Long> idSet = new HashSet<Long>();
        for (long id : ids) {
            idSet.add(id);
        }
        final Map<Long, T> idMap = factory.getStorage(clazz).getAllMap(idSet);
        final List<T> result = new ArrayList<>(idMap.size());
        for (long id : ids) {
            result.add(idMap.get(id));
        }
        return result;
    }

    public static <T extends IORMPartitioned> List<T> initializeListFromId(Class<T> clazz, long[] ids, long[] partitionIds, IORMFactory factory) {
        if (ids.length != partitionIds.length) {
            throw new RuntimeException("Id count mast be equals partitionIds count");
        }
        final Set<IORMPartitionKey> idSet = new LinkedHashSet<>();
        for (int i = 0; i < ids.length; i++) {
            idSet.add(new IORMPartitionKey(ids[i], partitionIds[i]));
        }
        final Map<IORMPartitionKey, T> idMap = factory.getStorage(clazz).getPartitionedAllMap(idSet);
        final List<T> result = new ArrayList<>(idMap.size());
        for (IORMPartitionKey id : idSet) {
            result.add(idMap.get(id));
        }
        return result;
    }

    public static Set<Long> getSetFromArray(long[] ids) {
        final Set<Long> result = new HashSet<Long>(ids.length);
        for (long id : ids) {
            if (id != Long.MIN_VALUE) {
                result.add(id);
            }
        }
        return result;
    }

    public static Set<IORMPartitionKey> getSetPartitionKeyFromArrays(long[] ids, long[] parts) {
        final Set<IORMPartitionKey> result = new HashSet<IORMPartitionKey>(ids.length);
        if (ids.length != parts.length) {
            throw new RuntimeException("Id length mast be equals patrs length");
        }
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] != Long.MIN_VALUE) {
                result.add(new IORMPartitionKey(ids[i], parts[i]));
            }
        }
        return result;
    }
}
