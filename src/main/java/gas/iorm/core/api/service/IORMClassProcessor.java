package gas.iorm.core.api.service;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import javassist.*;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ConstPool;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.SignatureAttribute;
import javassist.bytecode.annotation.*;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.util.*;

/**
 * Created by alex on 14.06.16.
 */
public class IORMClassProcessor {
//    private List<String> processed = new LinkedList<String>();
    private ClassPool cp = ClassPool.getDefault();
    private Set<String> processedClasses = new HashSet<String>();
    private String[] classPaths = new String[0];

    private Map<String, Map<String, String>> processedFields = new HashMap<String, Map<String, String>>();

    public long[] arrayPrototype;

    public IORMClassProcessor(String... classPaths)  {
        this.classPaths = classPaths;
    }

    public List<Class<?>> processClasses(String... classNames) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        for (String classPath : classPaths) {
            cp.insertClassPath(classPath);
        }
        final List<CtClass> classes = new LinkedList<CtClass>();
        for (String clazz : classNames) {
            classes.addAll(processClass(clazz));
        }
        final List<Class<?>> result = new LinkedList<Class<?>>();
        for (CtClass c : classes) {
            result.add(c.toClass());
        }
        return result;
    }

    private List<CtClass> processClass(String className) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        final List<CtClass> list = new LinkedList<CtClass>();
        if (this.processedClasses.contains(className)) {
            return list;
        }
        CtClass cl = cp.get(className);
        if (!implementsPersistable(cl) && !implementsPartitioned(cl)) {
            throw new NotFoundException("Class " + className + " not implement " + IORMPersistable.class.getCanonicalName() +
                " or " + IORMPartitioned.class.getCanonicalName());
        }
        CtClass parent = cl.getSuperclass();
        if (parent != null && (parent.getModifiers() & Modifier.ABSTRACT) == 0 &&
                !parent.getName().equals("java.lang.Object")) { //Родитель присутствует, не интерфейс и не абстрастный
            list.addAll(processClass(parent.getName()));
            //Добавление родительских полей в список обработанных
            final Map<String, String> parentProcessed = new HashMap<String, String>(this.processedFields.get(parent.getName()));
            this.processedFields.put(cl.getName(), parentProcessed);
        }
        addFactoryToClass(cl);
        final StringBuilder validationBuilder = new StringBuilder("public void ___validation () throws gas.iorm.core.api.EntityNotFoundException {\njava.util.Set idSet = null;");
        if (!list.isEmpty()) {
            validationBuilder.append("  super.___validation();\n");
        }
        CtField[] fields = cl.getDeclaredFields();
        for (CtField field : fields) {
            if (implementsPartitioned(field.getType())) {
                processPartitionedField(cl, field, validationBuilder);
            } else if (implementsPersistable(field.getType())) {
                processField(cl, field, validationBuilder);
            }

            final CtClass listType = isListOfPersistable(field);
            if (listType != null) {
                list.addAll(processClass(listType.getName()));
//                Обрабоотка листа
                if (implementsPartitioned(listType)) {
                    processPartitionedListField(cl, field, listType, validationBuilder);
                } else if (implementsPersistable(listType)) {
                    processListField(cl, field, listType, validationBuilder);
                }
            }
        }
        list.add(cl);
        validationBuilder.append("}");
        CtMethod validation = CtNewMethod.make(validationBuilder.toString(), cl);
        cl.addMethod(validation);
        this.processedClasses.add(cl.getName());
        return list;
    }

    private void processPartitionedListField(CtClass cl, CtField listField, CtClass listType, StringBuilder validationBuilder) throws NotFoundException, CannotCompileException {
        cl.removeField(listField);
        CtField prototype = cp.get(this.getClass().getCanonicalName()).getField("arrayPrototype");
        CtField replaced = new CtField(prototype, cl);
        replaced.setName(listField.getName());
        CtField replacedPartition = new CtField(prototype, cl);
        replacedPartition.setName(listField.getName() + "___PartitionKeys");
        cl.addField(replaced, "new long[0]");
        cl.addField(replacedPartition, "new long[0]");
        listField.setName("___" + replaced.getName());
        listField.setModifiers(listField.getModifiers() | Modifier.PRIVATE | Modifier.TRANSIENT);
        cl.addField(listField);

        preparePartitionedListGetter(cl, listField, replaced, replacedPartition, listType);
        preparePartitionedListSetter(cl, listField, replaced, replacedPartition, listType);

        validationBuilder.append("" +
                "   idSet = gas.iorm.core.api.service.IORMListInitializer.getSetPartitionKeyFromArrays(this." + replaced.getName() + ", this." + replacedPartition.getName() + ");" +
                "   if (!" + cl.getName() + ".___factory.getStorage(" + listType.getName() + ".class).containsPartitionedKey(idSet)) {\n" +
                "       throw new gas.iorm.core.api.EntityNotFoundException(\"Classes " + listType.getName() + " with id \" + idSet.toString() + \" not found in storage.\");\n" +
                "   }\n");
    }

    private void processListField(CtClass cl, CtField listField, CtClass listType, StringBuilder validationBuilder) throws NotFoundException, CannotCompileException {
        cl.removeField(listField);
        CtField prototype = cp.get(this.getClass().getCanonicalName()).getField("arrayPrototype");
        CtField replaced = new CtField(prototype, cl);
        replaced.setName(listField.getName());
        cl.addField(replaced, "new long[0]");
        listField.setName("___" + replaced.getName());
        listField.setModifiers(listField.getModifiers() | Modifier.PRIVATE | Modifier.TRANSIENT);
        cl.addField(listField);

        prepareListGetter(cl, listField, replaced, listType);
        prepareListSetter(cl, listField, replaced, listType);

        validationBuilder.append("" +
                "   idSet = gas.iorm.core.api.service.IORMListInitializer.getSetFromArray(this." + replaced.getName() + ");" +
                "   if (!" + cl.getName() + ".___factory.getStorage(" + listType.getName() + ".class).containsKey(idSet)) {\n" +
                "       throw new gas.iorm.core.api.EntityNotFoundException(\"Classes " + listType.getName() + " with id \" + idSet.toString() + \" not found in storage.\");\n" +
                "   }\n");

    }

    private void preparePartitionedListGetter(CtClass cl, CtField listField, CtField replaced, CtField replacedPartitioned, CtClass listType) throws NotFoundException, CannotCompileException {
        final String getter = getterName(replaced);
        final CtMethod getterMethod = cl.getDeclaredMethod(getter, null);
        final String body = "{\n" +
                "   if (this." + listField.getName() + " == null) {\n" +
                "       java.util.List idsList = gas.iorm.core.api.service.IORMListInitializer.initializeListFromId(" + listType.getName() + ".class, this." + replaced.getName() + ", this." + replacedPartitioned.getName() + ", " + cl.getName() + ".___factory);\n" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMListPartitioned(idsList, \"" + replaced.getName() + "\", this);\n" +
                "   }\n" +
                "   return this." + listField.getName() + ";\n" +
                "}";
        getterMethod.setBody(body);
    }

    private void preparePartitionedListSetter(CtClass cl, CtField listField, CtField replaced, CtField replacedPartitioned, CtClass listType) throws NotFoundException, CannotCompileException {
        final String setter = setterName(replaced);
        final CtClass[] params = new CtClass[] {listField.getType()};
        final CtMethod setterMethod = cl.getDeclaredMethod(setter, params);
        setterMethod.setBody("{\n" +
                "   if ($1 == null) {" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMListPartitioned(\"" + replaced.getName() + "\", this);\n" +
                "       this." + replaced.getName() + " = new long[0];" +
                "       this." + replacedPartitioned.getName() + " = new long[0];" +
                "   } else {" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMListPartitioned($1,\"" + replaced.getName() + "\", this);\n" +
                "   }" +
                "}");
    }

    private void prepareListGetter(CtClass cl, CtField listField, CtField replaced, CtClass listType) throws NotFoundException, CannotCompileException {
        final String getter = getterName(replaced);
        final CtMethod getterMethod = cl.getDeclaredMethod(getter, null);
        final String body = "{\n" +
                "   if (this." + listField.getName() + " == null) {\n" +
                "       java.util.List idsList = gas.iorm.core.api.service.IORMListInitializer.initializeListFromId(" + listType.getName() + ".class, this." + replaced.getName() + ", " + cl.getName() + ".___factory);\n" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMList(idsList, \"" + replaced.getName() + "\", this);\n" +
                "   }\n" +
                "   return this." + listField.getName() + ";\n" +
                "}";
        getterMethod.setBody(body);
    }

    private void prepareListSetter(CtClass cl, CtField listField, CtField replaced, CtClass listType) throws NotFoundException, CannotCompileException {
        final String setter = setterName(replaced);
        final CtClass[] params = new CtClass[] {listField.getType()};
        final CtMethod setterMethod = cl.getDeclaredMethod(setter, params);
        setterMethod.setBody("{\n" +
                "   if ($1 == null) {" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMList(\"" + replaced.getName() + "\", this);\n" +
                "       this." + replaced.getName() + " = new long[0];" +
                "   } else {" +
                "       this." + listField.getName() + " = new gas.iorm.core.api.entity.IORMList($1,\"" + replaced.getName() + "\", this);\n" +
                "   }" +
                "}");
    }

    private void addQuerySqlFieldAnnotationToField(QuerySqlField source, CtClass cl, CtField field) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        final ConstPool constPool = cl.getClassFile().getConstPool();
        final AnnotationsAttribute attr = new AnnotationsAttribute(constPool, AnnotationsAttribute.visibleTag);
        final Annotation annotation = new Annotation(QuerySqlField.class.getCanonicalName(), constPool);
        annotation.addMemberValue("index", new BooleanMemberValue(source.index(), constPool));
        annotation.addMemberValue("descending", new BooleanMemberValue(source.descending(), constPool));

        ArrayMemberValue amv = new ArrayMemberValue(constPool);
        String[] groups = source.groups();
        MemberValue[] groupValues = new MemberValue[groups.length];
        for (int i = 0; i < groups.length; i++) {
            groupValues[i] = new StringMemberValue(groups[i], constPool);
        }
        amv.setValue(groupValues);
        annotation.addMemberValue("groups", amv);

        ArrayMemberValue orderGrp = new ArrayMemberValue(constPool);
        QuerySqlField.Group[] orderGroups = source.orderedGroups();
        MemberValue[] orderValues = new MemberValue[orderGroups.length];
        for (int i = 0; i < orderGroups.length; i++) {
            Annotation orderAnn = new Annotation("org.apache.ignite.cache.query.annotations.QuerySqlField$Group", constPool);
            orderAnn.addMemberValue("name", new StringMemberValue(orderGroups[i].name(), constPool));
            IntegerMemberValue imv = new IntegerMemberValue(constPool);
            imv.setValue(orderGroups[i].order());
            orderAnn.addMemberValue("order", imv);
            orderAnn.addMemberValue("descending", new BooleanMemberValue(orderGroups[i].descending(), constPool));
            orderValues[i] = new AnnotationMemberValue(orderAnn, constPool);
        }
        orderGrp.setValue(orderValues);
        annotation.addMemberValue("orderedGroups", orderGrp);

        annotation.addMemberValue("name", new StringMemberValue(source.name(), constPool));

        attr.addAnnotation(annotation);
        field.getFieldInfo().addAttribute(attr);
    }

    private void moveAnnotationSqlField(CtClass cl, CtField fieldFrom, CtField fieldTo) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        final QuerySqlField querySQL = (QuerySqlField)fieldFrom.getAnnotation(QuerySqlField.class);
        if (querySQL != null) {
//            final ConstPool constPool = cl.getClassFile().getConstPool();
            addQuerySqlFieldAnnotationToField(querySQL, cl, fieldTo);
            //Убираем станую анатацию
            final Iterator iter = fieldFrom.getFieldInfo().getAttributes().iterator();
            while(iter.hasNext()) {
                if (iter.next() instanceof AnnotationsAttribute) {
                    iter.remove();
                }
            }

        }
    }

    private void processPartitionedField(CtClass cl, CtField field, StringBuilder validationBuilder) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        cl.removeField(field);
        CtField replaced = new CtField(CtClass.longType, field.getName(), cl);
        CtField replacedPartition = new CtField(CtClass.longType, field.getName() + "___partitionKey" , cl);
        replaced.setModifiers(replaced.getModifiers() | Modifier.PRIVATE);
        moveAnnotationSqlField(cl, field, replaced);
        cl.addField(replaced, "Long.MIN_VALUE");
        cl.addField(replacedPartition, "Long.MIN_VALUE");
        field.setName("___" + replaced.getName());
        field.setModifiers(field.getModifiers() | Modifier.PRIVATE | Modifier.TRANSIENT);
        cl.addField(field);

        preparePartitionedGetter(cl, field, replaced, replacedPartition);
        preparePartitionalSetter(cl, field, replaced, replacedPartition);


        validationBuilder.append("" +
                "   if (this." + replaced.getName() + " != Long.MIN_VALUE && this." + replacedPartition.getName() + " != Long.MIN_VALUE && !" + cl.getName() + ".___factory.getStorage(" + field.getType().getName() + ".class).containsKey(new gas.iorm.core.api.entity.IORMPartitionKey(this." + replaced.getName() + ", this." + replacedPartition.getName() + "))) {\n" +
                "       throw new gas.iorm.core.api.EntityNotFoundException(\"Class " + field.getType().getName() + " with id \" + this." + replaced.getName() + " + \" not found in storage.\");\n" +
                "   }\n");
        //Добавление в список обработаных полей
        if (!processedFields.containsKey(cl.getName())) {
            processedFields.put(cl.getName(), new HashMap<String, String>());
        }
        processedFields.get(cl.getName()).put(replaced.getName(), field.getType().getName());

    }

    private void processField(CtClass cl, CtField field, StringBuilder validationBuilder) throws ClassNotFoundException, NotFoundException, CannotCompileException {
        cl.removeField(field);
        CtField replaced = new CtField(CtClass.longType, field.getName(), cl);
        replaced.setModifiers(replaced.getModifiers() | Modifier.PRIVATE);
        moveAnnotationSqlField(cl, field, replaced);
        cl.addField(replaced, "Long.MIN_VALUE");
        field.setName("___" + replaced.getName());
        field.setModifiers(field.getModifiers() | Modifier.PRIVATE | Modifier.TRANSIENT);
        cl.addField(field);

        prepareGetter(cl, field, replaced);
        prepareSetter(cl, field, replaced);
        validationBuilder.append("" +
                "   if (this." + replaced.getName() + " != Long.MIN_VALUE && !" + cl.getName() + ".___factory.getStorage(" + field.getType().getName() + ".class).containsKey(this." + replaced.getName() + ")) {\n" +
                "       throw new gas.iorm.core.api.EntityNotFoundException(\"Class " + field.getType().getName() + " with id \" + this." + replaced.getName() + " + \" not found in storage.\");\n" +
                "   }\n");
        //Добавление в список обработаных полей
        if (!processedFields.containsKey(cl.getName())) {
            processedFields.put(cl.getName(), new HashMap<String, String>());
        }
        processedFields.get(cl.getName()).put(replaced.getName(), field.getType().getName());
    }

    private void addFactoryToClass(CtClass cl) throws NotFoundException, CannotCompileException {
        final CtClass factoryClass = cp.get(IORMFactory.class.getCanonicalName());
        CtField factory = new CtField(factoryClass, "___factory", cl);
        factory.setModifiers(Modifier.PUBLIC | Modifier.STATIC);
        cl.addField(factory);
    }

    private void preparePartitionedGetter(CtClass cl, CtField field, CtField replaced, CtField replacedPartitioned) throws NotFoundException, CannotCompileException {
        final String getter = getterName(replaced);
        final CtMethod getterMethod = cl.getDeclaredMethod(getter, null);
        getterMethod.setBody("{\n" +
                "   if (this." + field.getName() + " == null && " + replaced.getName() + " != Long.MIN_VALUE) {\n" +
                "       " + field.getName() + " = (" + field.getType().getName() + ")" + cl.getName() + ".___factory.getStorage(" + field.getType().getName() + ".class).get(new gas.iorm.core.api.entity.IORMPartitionKey(this." + replaced.getName() + ", this." + replacedPartitioned.getName() + "));\n" +
                "   }\n" +
                "   return this." + field.getName() + ";\n" +
                "}");
    }

    private void preparePartitionalSetter(CtClass cl, CtField field, CtField replaced, CtField replacedPartitioned) throws NotFoundException, CannotCompileException {
        final String setter = setterName(replaced);
        final CtClass params[] = new CtClass[] {field.getType()};
        final CtMethod setterMethod = cl.getDeclaredMethod(setter, params);
        setterMethod.setBody("{\n" +
                "   if ($1 == null) {\n" +
                "       this." + replaced.getName() + " = Long.MIN_VALUE;\n" +
                "       this." + replacedPartitioned.getName() + " = Long.MIN_VALUE;\n" +
                "       this." + field.getName() + " = null;\n" +
                "   } else {\n" +
                "       this." + replaced.getName() + " = $1.getId();\n" +
                "       this." + replacedPartitioned.getName() + " = $1.getAffinity().getId();\n" +
                "       this." + field.getName() + " = $1;\n" +
                "   }\n" +
                "}");
    }

    private void prepareGetter(CtClass cl, CtField field, CtField replaced) throws NotFoundException, CannotCompileException {
        final String getter = getterName(replaced);
        final CtMethod getterMethod = cl.getDeclaredMethod(getter, null);
        getterMethod.setBody("{\n" +
                "   if (this." + field.getName() + " == null && " + replaced.getName() + " != Long.MIN_VALUE) {\n" +
                "       " + field.getName() + " = (" + field.getType().getName() + ")" + cl.getName() + ".___factory.getStorage(Class.forName(\"" + field.getType().getName() + "\")).get(this." + replaced.getName() + ");\n" +
                "   }\n" +
                "   return this." + field.getName() + ";\n" +
                "}");
    }

    private void prepareSetter(CtClass cl, CtField field, CtField replaced) throws NotFoundException, CannotCompileException {
        final String setter = setterName(replaced);
        final CtClass params[] = new CtClass[] {field.getType()};
        final CtMethod setterMethod = cl.getDeclaredMethod(setter, params);
        setterMethod.setBody("{\n" +
                "   if ($1 == null) {\n" +
                "       this." + replaced.getName() + " = Long.MIN_VALUE;\n" +
                "       this." + field.getName() + " = null;\n" +
                "   } else {\n" +
                "       this." + replaced.getName() + " = $1.getId();\n" +
                "       this." + field.getName() + " = $1;\n" +
                "   }\n" +
                "}");
    }

    private String getterName(CtField field) throws NotFoundException {
        final String typeName = field.getType().getName();
        if (typeName.equals("boolean")) {
            return "is" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        } else {
            return "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
        }
    }

    private String setterName(CtField field) {
        return "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);
    }

    private boolean implementsPersistable(CtClass clazz) throws NotFoundException {
        for (CtClass c : clazz.getInterfaces()) {
            if (c.getName().equals(IORMPersistable.class.getCanonicalName())) {
                return true;
            }
        }
        final CtClass parent = clazz.getSuperclass();
        if (parent != null) {
            return implementsPersistable(parent);
        } else {
            return false;
        }
    }

    private boolean implementsPartitioned(CtClass clazz) throws NotFoundException {
        for (CtClass c : clazz.getInterfaces()) {
            if (c.getName().equals(IORMPartitioned.class.getCanonicalName())) {
                return true;
            }
        }
        final CtClass parent = clazz.getSuperclass();
        if (parent != null) {
            return implementsPartitioned(parent);
        } else {
            return false;
        }
    }

    private CtClass isListOfPersistable(CtField field) throws NotFoundException {
        if (field.getType().getName().equals(List.class.getCanonicalName())) {
            FieldInfo fin = field.getFieldInfo();
            List<SignatureAttribute> attributes = fin.getAttributes();
            if (attributes.isEmpty()) {
                throw new NotFoundException("Signature of List field with name '" + field.getName() + "' not found");
            }
            final String className = getClassNameFromListSignature(attributes.get(0).getSignature());
            final CtClass clazz = cp.get(className);
            if (implementsPersistable(clazz) || implementsPartitioned(clazz)) {
                return clazz;
            }
        }
        return null;
    }

    private String getClassNameFromListSignature(String signature) {
        if (signature.startsWith("Ljava/util/List<L")) {
            return signature.substring(17, signature.length() - 3).replace("/", ".");
        } else {
            return null;
        }
    }

    public Class<?> getFieldType(Class<?> clazz, String fieldName) {
        try {
            if (processedFields.containsKey(clazz.getCanonicalName()) &&
                    processedFields.get(clazz.getCanonicalName()).containsKey(fieldName)) {
                return Class.forName(processedFields.get(clazz.getCanonicalName()).get(fieldName));
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
