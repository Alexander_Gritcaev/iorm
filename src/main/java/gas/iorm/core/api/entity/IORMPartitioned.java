package gas.iorm.core.api.entity;

/**
 * Created by alex on 10.08.16.
 */
public interface IORMPartitioned<T extends IORMPersistable> extends IORMPersistable {
    T getAffinity();
}
