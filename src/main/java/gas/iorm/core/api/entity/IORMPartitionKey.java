package gas.iorm.core.api.entity;

import org.apache.ignite.cache.affinity.AffinityKeyMapped;

/**
 * Created by alex on 16.08.16.
 */
public class IORMPartitionKey<T extends IORMPartitioned> {
    private long id;
    @AffinityKeyMapped
    private long partitionId;

    public IORMPartitionKey() {
    }

    public IORMPartitionKey(long id, long partitionId) {
        this.id = id;
        this.partitionId = partitionId;
    }

    public long getId() {
        return id;
    }

    public long getPartitionId() {
        return partitionId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPartitionId(long partitionId) {
        this.partitionId = partitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IORMPartitionKey<?> that = (IORMPartitionKey<?>) o;

        if (id != that.id) return false;
        return partitionId == that.partitionId;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (partitionId ^ (partitionId >>> 32));
        return result;
    }
}
