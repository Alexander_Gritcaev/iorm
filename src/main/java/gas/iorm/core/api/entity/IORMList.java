package gas.iorm.core.api.entity;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Created by alex on 17.06.16.
 */
public class IORMList<T extends IORMPersistable> extends ArrayList<T> {

    private transient Field idsField;
    private transient IORMPersistable object;

    protected void updateArray(long[] array) {
        try {
            idsField.set(object, array);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Cannot set field " + idsField.getName());
        }
    }

    private void update() {
        final long[] array = new long[size()];
        for (int i = 0; i < size(); i++) {
            array[i] = get(i).getId();
        }
        updateArray(array);
    }

    @Override
    public T set(int index, T element) {
        final T res = super.set(index, element);
        update();
        return res;
    }

    public IORMList(String fieldName, IORMPersistable object) {
        super();
        try {
            this.idsField = object.getClass().getDeclaredField(fieldName);
            this.object = object;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Field " + fieldName + " not found");
        }
    }

    public IORMList(Collection<? extends T> c, String fieldName, IORMPersistable object) {
        super(c);
        try {
            this.idsField = object.getClass().getDeclaredField(fieldName);
            this.object = object;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Field " + fieldName + " not found");
        }
        update();
    }

    @Override
    public boolean add(T t) {
        boolean res = super.add(t);
        update();
        return res;
    }

    @Override
    public void add(int index, T element) {
        super.add(index, element);
        update();
    }

    @Override
    public T remove(int index) {
        final T res = super.remove(index);
        update();
        return res;
    }

    @Override
    public boolean remove(Object o) {
        final boolean res = super.remove(o);
        update();
        return res;
    }

    @Override
    public void clear() {
        super.clear();
        update();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        final boolean res = super.addAll(c);
        update();
        return res;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        final boolean res = super.addAll(index, c);
        update();
        return res;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        update();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        final boolean res = super.removeAll(c);
        update();
        return res;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        final boolean res = super.retainAll(c);
        update();
        return res;
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator(int index) {
        return new IORMLIstIter<>(super.listIterator(index), this);
//        throw new UnsupportedOperationException("This operation is not implemented");
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator() {
        return new IORMLIstIter<>(super.listIterator(), this);
//        throw new UnsupportedOperationException("This operation is not implemented");
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
//        throw new UnsupportedOperationException("This operation is not implemented");
        return new IORMIter<>(super.iterator(), this);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return Collections.unmodifiableList(super.subList(fromIndex, toIndex));
    }

    @Override
    public Spliterator<T> spliterator() {
        throw new UnsupportedOperationException("This operation is not implemented");
//        return super.spliterator();
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        final boolean res = super.removeIf(filter);
        update();
        return res;
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        super.replaceAll(operator);
        update();
    }

    @Override
    public void sort(Comparator<? super T> c) {
        super.sort(c);
        update();
    }

    private static class IORMIter<T extends IORMPersistable> implements Iterator<T> {

        private Iterator<T> iterator;
        IORMList<T> list;

        public IORMIter(Iterator<T> iterator, IORMList<T> list) {
            this.iterator = iterator;
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public T next() {
            return iterator.next();
        }

        @Override
        public void remove() {
            iterator.remove();
            list.update();
        }

        @Override
        public void forEachRemaining(Consumer<? super T> action) {
            iterator.forEachRemaining(action);
        }
    }

    private static class IORMLIstIter<T extends IORMPersistable> implements ListIterator<T> {
        private ListIterator<T> iterator;
        private IORMList<T> list;

        public IORMLIstIter(ListIterator<T> iterator, IORMList<T> list) {
            this.iterator = iterator;
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public T next() {
            return iterator.next();
        }

        @Override
        public boolean hasPrevious() {
            return iterator.hasPrevious();
        }

        @Override
        public T previous() {
            return iterator.previous();
        }

        @Override
        public int nextIndex() {
            return iterator.nextIndex();
        }

        @Override
        public int previousIndex() {
            return iterator.previousIndex();
        }

        @Override
        public void remove() {
            iterator.remove();
            list.update();
        }

        @Override
        public void set(T t) {
            iterator.set(t);
            list.update();
        }

        @Override
        public void add(T t) {
            iterator.add(t);
            list.update();
        }

        @Override
        public void forEachRemaining(Consumer<? super T> action) {
            iterator.forEachRemaining(action);
        }
    }
}
