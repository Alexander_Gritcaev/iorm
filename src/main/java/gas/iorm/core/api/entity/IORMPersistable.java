package gas.iorm.core.api.entity;

import java.io.Serializable;

/**
 * Created by alex on 10.06.16.
 */
public interface IORMPersistable extends Serializable {
    long getId();
    void setId(long id);
}
