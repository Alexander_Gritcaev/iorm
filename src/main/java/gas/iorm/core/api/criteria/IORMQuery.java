package gas.iorm.core.api.criteria;

import gas.iorm.core.api.entity.IORMPersistable;

import java.util.List;

/**
 * Created by alex on 28.06.16.
 */
public interface IORMQuery<T extends IORMPersistable> {
    IORMQuery<T> setParameter(String name, Object value);
    List<T> execute();
}
