package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.criteria.IORMQuery;
import gas.iorm.core.api.entity.IORMPersistable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 28.06.16.
 */
public class IORMQueryImpl<T extends IORMPersistable> implements IORMQuery<T> {

    private Class<T> clazz;
    private String directQuery;
    private IORMFactory factory;
    private ParamOrder paramOrder;
    private Map<String, Object> parameters = new HashMap<String, Object>();

    public IORMQueryImpl(Class<T> clazz, String directQuery, IORMFactory factory, ParamOrder paramOrder) {
        this.directQuery = directQuery;
        this.factory = factory;
        this.clazz = clazz;
        this.paramOrder = paramOrder;
    }

    public IORMQueryImpl<T> setParameter(String name, Object value) {
        this.parameters.put(name, value);
        return this;
    }

    public List<T> execute() {
        final List<String> inOrder = paramOrder.getParamsIn();
        final List<String> order = paramOrder.getParams();
        final Object[] args = new Object[inOrder.size() + order.size()];
        int position = 0;
        for (String name : inOrder) {
            if (!parameters.containsKey(name)) {
                throw new RuntimeException("Parameter with name '" + name + "' not found");
            }
            args[position++] = parameters.get(name);
        }
        for (String name : order) {
            if (!parameters.containsKey(name)) {
                throw new RuntimeException("Parameter with name '" + name + "' not found");
            }
            args[position++] = parameters.get(name);
        }
        return factory.getStorage(clazz).executeQuery(clazz, directQuery, args);
    }
}
