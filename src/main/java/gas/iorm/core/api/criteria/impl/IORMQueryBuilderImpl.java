package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.criteria.IORMExpression;
import gas.iorm.core.api.criteria.IORMQuery;
import gas.iorm.core.api.criteria.IORMQueryBuilder;
import gas.iorm.core.api.criteria.IORMSort;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public class IORMQueryBuilderImpl<T extends IORMPersistable> implements IORMQueryBuilder<T> {
    private Class<T> clazz;
    private IORMFactory factory;

    private IORMExpressionImpl<T> expression;
    private IORMSortImpl<T> sort;

    public IORMQueryBuilderImpl(Class<T> clazz, IORMFactory factory) {
        this.clazz = clazz;
        this.factory = factory;
        if (!factory.containClass(clazz)) {
            throw new RuntimeException("Class '" + clazz.getCanonicalName() + "' not added to factory");
        }
    }


    public IORMExpression<T> where() {
        this.expression = new IORMExpressionImpl<T>(this);
        return this.getExpression();
    }

    public IORMExpression<T> complex() {
        return new IORMExpressionImpl<T>(this);
    }

    public IORMSort<T> orderBy() {
        this.sort = new IORMSortImpl<T>(this);
        return this.getSort();
    }

    public void reset() {
        this.expression = null;
        this.sort = null;
    }

    public IORMQuery<T> toQuery() {
        return QueryBuilder.toQuery(this);
    }

    Class<T> getClazz() {
        return clazz;
    }

    IORMFactory getFactory() {
        return factory;
    }

    IORMExpressionImpl<T> getExpression() {
        return expression;
    }

    IORMSortImpl<T> getSort() {
        return sort;
    }
}
