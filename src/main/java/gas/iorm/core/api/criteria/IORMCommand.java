package gas.iorm.core.api.criteria;

import gas.iorm.core.api.entity.IORMPersistable;

import java.util.List;

/**
 * Created by alex on 24.06.16.
 */
public interface IORMCommand<T extends IORMPersistable, V> {
    IORMCondition<T> eq(V value);
    IORMCondition<T> eqp(String paramName);
    IORMCondition<T> gt(V value);
    IORMCondition<T> gtp(String paramName);
    IORMCondition<T> gq(V value);
    IORMCondition<T> gqp(String paramName);
    IORMCondition<T> lt(V value);
    IORMCondition<T> ltp(String paramName);
    IORMCondition<T> lq(V value);
    IORMCondition<T> lqp(String paramName);
    IORMCondition<T> neq(V value);
    IORMCondition<T> neqp(String paramName);
    IORMCondition<T> inp(String paramName);
    IORMCondition<T> like(String value);
    IORMCondition<T> likep(String paramName);
    IORMCondition<T> isNull();
    IORMCondition<T> isNotNull();
}
