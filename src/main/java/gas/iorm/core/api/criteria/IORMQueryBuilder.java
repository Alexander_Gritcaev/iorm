package gas.iorm.core.api.criteria;

import gas.iorm.core.api.criteria.IORMExpression;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public interface IORMQueryBuilder<T extends IORMPersistable>  {
    IORMExpression<T> where();
    IORMExpression<T> complex();
    IORMSort<T> orderBy();
    IORMQuery<T> toQuery();
}
