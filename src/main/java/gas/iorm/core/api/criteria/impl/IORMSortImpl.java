package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.api.criteria.IORMQuery;
import gas.iorm.core.api.criteria.IORMSort;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 12.07.16.
 */
public class IORMSortImpl<T extends IORMPersistable> implements IORMSort<T> {

    private IORMQueryBuilderImpl<T> queryBuilder;

    private String field;
    private Order order;
    private IORMSortImpl<T> nextOrder;

    public IORMSortImpl(IORMQueryBuilderImpl<T> builder) {
        this.queryBuilder = builder;
    }

    public IORMSort<T> order(String field, Order order) {
        this.field = field;
        this.order = order;
        this.nextOrder = new IORMSortImpl<T>(this.queryBuilder);
        return nextOrder;
    }

    public IORMQuery<T> toQuery() {
        return QueryBuilder.toQuery(this.queryBuilder);
    }

    String getField() {
        return field;
    }

    Order getOrder() {
        return order;
    }

    IORMSortImpl<T> getNextOrder() {
        return nextOrder;
    }
}
