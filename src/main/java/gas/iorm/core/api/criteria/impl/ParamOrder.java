package gas.iorm.core.api.criteria.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 11.07.16.
 */
class ParamOrder {

    private List<String> params = new LinkedList<String>();
    private List<String> paramsIn = new LinkedList<String>();

    public void addParameter(String paramName) {
        this.params.add(paramName);
    }

    public void addInParameter(String paramName) {
        this.paramsIn.add(paramName);
    }

    public List<String> getParams() {
        return Collections.unmodifiableList(params);
    }

    public List<String> getParamsIn() {
        return Collections.unmodifiableList(paramsIn);
    }
}
