package gas.iorm.core.api.criteria;

import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 12.07.16.
 */
public interface IORMSort<T extends IORMPersistable> {
    IORMSort<T> order(String field, Order order);
    IORMQuery<T> toQuery();

    public enum Order {
        ASC, DESC;
    }
}
