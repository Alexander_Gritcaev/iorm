package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.criteria.IORMCommand;
import gas.iorm.core.api.criteria.IORMCondition;
import gas.iorm.core.api.criteria.IORMExpression;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public class IORMExpressionImpl<T extends IORMPersistable> implements IORMExpression<T> {

    private IORMQueryBuilderImpl<T> queryBuilder;

    private String fieldName;
    private Class<?> fieldType;
    private IORMCommandImpl<T, ?> command;
    private IORMConditionImpl<T> condition;
    private Expression expression;
    private IORMExpressionImpl<T> complexExpression;

    public IORMExpressionImpl(IORMQueryBuilderImpl<T> queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    @SuppressWarnings("unchecked")
    public <V> IORMCommand<T, V> field(String fieldName, Class<V> fieldType) {
        checkField(fieldName, fieldType);
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.command = new IORMCommandImpl<T, V>(this.queryBuilder, fieldType);
        this.setExpression(Expression.FIELD);
        return (IORMCommand<T, V>) getCommand();
    }

    private <V> void checkField(String field, Class<V> fieldType) {
        final IORMFactory factory = this.queryBuilder.getFactory();
        final String[] paths = field.split("\\.");
        Class<?> cls = this.queryBuilder.getClazz();
        for (String path : paths) {
            final Class<?> type = factory.getOriginalFieldType(cls, path);
            if (type == null) {
                throw new RuntimeException("Field '" + path + "' in class '" + cls.getCanonicalName() + "' not found");
            }
            cls = type;
        }
        if (cls != fieldType) {
            throw new RuntimeException("Field '" + field + "' is not a '" + fieldType.getCanonicalName() + "' type");
        }
    }

    public IORMCondition<T> complex(IORMExpression<T> complex) {
        this.setExpression(Expression.COMPLEX);
        this.complexExpression = (IORMExpressionImpl<T>)complex;
        this.condition = new IORMConditionImpl<T>(this.queryBuilder);
        return this.getCondition();
    }

    String getFieldName() {
        return fieldName;
    }

    Class<?> getFieldType() {
        return fieldType;
    }

    IORMCommandImpl<T, ?> getCommand() {
        return command;
    }

    Expression getExpression() {
        return expression;
    }

    void setExpression(Expression expression) {
        this.expression = expression;
    }

    IORMExpressionImpl<T> getComplexExpression() {
        return complexExpression;
    }

    IORMConditionImpl<T> getCondition() {
        return condition;
    }

    enum Expression {
        FIELD, COMPLEX
    }
}
