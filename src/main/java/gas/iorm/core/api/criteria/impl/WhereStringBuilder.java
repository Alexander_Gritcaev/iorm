package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.entity.IORMPersistable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 28.06.16.
 */
class WhereStringBuilder {

    public static <T extends IORMPersistable> void buildWhereSort(final StringBuilder sb, Class<?> baseClass,
                                            IORMSortImpl<T> sort, IORMFactory factory, List<Class<?>> process) {
        final List<Class<?>> processed = process == null ? new LinkedList<Class<?>>() : process;
        IORMSortImpl srt = sort;
        boolean needClose = false;

        while(srt != null && srt.getField() != null) {
            final String field = srt.getField();
            final String[] paths = field.split("\\.");
            Class<?> cls = baseClass;
            for (String path : paths) {
                final Class<?> type = factory.getOriginalFieldType(cls, path);
                if (ReflectionFelper.implementsInterface(type, IORMPersistable.class)) {
                    if (!processed.contains(type)) {
                        if (!needClose) {
                            sb.append("( ");
                            needClose = true;
                        } else {
                            sb.append("AND ");
                        }
                        sb.append(cls.getSimpleName()).append(".").append(path).append(" = ")
                                .append(type.getSimpleName()).append(".id ");
                        processed.add(type);

                    }
                    cls = type;
                }
            }
            srt = srt.getNextOrder();
        }
        if (needClose) {
            sb.append(") ");
        }
    }

    public static <T extends IORMPersistable>  void buildWhereCondition(final StringBuilder sb, Class<?> baseClass,
                                            IORMExpressionImpl<T> expression, IORMFactory factory, List<Class<?>> process,
                                            ParamOrder paramOrder) {
//         sb.append("WHERE ");
         IORMExpressionImpl<T> exp = expression;
         final List<Class<?>> processed = process == null ? new LinkedList<Class<?>>() : process;
         processed.add(baseClass);

         while(exp != null) {
             if (exp.getExpression() == IORMExpressionImpl.Expression.COMPLEX) {
                 sb.append(" (");
                 buildWhereCondition(sb, baseClass, exp.getComplexExpression(), factory, processed, paramOrder);
                 sb.append(") ");
                 if (exp.getCondition() != null) {
                     processCondition(exp.getCondition(), sb, baseClass, factory);
                     exp = exp.getCondition().getExpression();
                     if (exp == null) {
                         continue;
                     }
                 } else {
                     exp = null;
                     continue;
                 }
             }
             if (exp.getExpression() == IORMExpressionImpl.Expression.FIELD) {
                 final String field = exp.getFieldName();
                 final String[] paths = field.split("\\.");
                 Class<?> cls = baseClass;
                 boolean isPersistable = false;
                 for (String path : paths) {
                     final Class<?> type = factory.getOriginalFieldType(cls, path);
                     if (ReflectionFelper.implementsInterface(type, IORMPersistable.class)) {
                         if (!processed.contains(type)) {
                             if (!isPersistable) {
                                 sb.append("(");
                             }
                             sb.append(cls.getSimpleName()).append(".").append(path).append(" = ")
                                     .append(type.getSimpleName()).append(".id AND ");
                             processed.add(type);
                             isPersistable = true;

                         }
                         cls = type;
                     }
                 }
                 //Если IN, то ппустое условие, чтобы не менять ранее внесенный текст,
                 //так как уже должен быть в конструкции FROM
                 if (exp.getCommand().getCommand() == IORMCommandImpl.Command.INP) {
                     sb.append("1=1 ");
                 } else {
                     sb.append(cls.getSimpleName()).append(".").append(paths[paths.length - 1]).append(" ");
                 }

                 if (exp.getCommand() != null) {
                     processCommand(exp.getCommand(), sb, paramOrder);
                     if (isPersistable) {
                         sb.append(") ");
                     }
                     if (exp.getCommand().getCondition() != null) {
                         processCondition(exp.getCommand().getCondition(), sb, baseClass, factory);
                     }
                 }
                 if (exp.getCommand() != null && exp.getCommand().getCommand() != null &&
                         exp.getCommand().getCondition() != null && exp.getCommand().getCondition().getCondition() != null) {
                     exp = exp.getCommand().getCondition().getExpression();
                 } else {
                     exp = null;
                 }
             }
         }
    }

    private static <T extends IORMPersistable> void processCondition(IORMConditionImpl<T> condition, StringBuilder sb,
                                                                     Class<?> baseClass, IORMFactory factory) {
        final IORMConditionImpl.Condition cond = condition.getCondition();
        if (cond != null) {
            switch (cond) {
                case AND:
                    sb.append("AND ");
                    break;
                case OR:
                    sb.append("OR ");
                    break;
            }
        }
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    private static <T extends IORMPersistable, V> void processCommand(IORMCommandImpl<T, V> command, StringBuilder sb,
                                                                      ParamOrder paramOrder) {
        final IORMCommandImpl.Command cmd = command.getCommand();
        final V value = command.getValue();
        final String likeString = command.getLikeString();
        final boolean isString = (value != null && value.getClass().equals(String.class));
        final boolean isDate = (value instanceof Date);
        final Date dateValue = isDate ? (Date)value : null;

        switch(cmd) {
            case EQ:
                if (isString) {
                    sb.append("= '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append("= PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append("= ").append(value).append(" ");
                break;
            case EQP:
                sb.append("= ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case GQ:
                if (isString) {
                    sb.append(">= '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append(">= PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append(">= ").append(value).append(" ");
                break;
            case GQP:
                sb.append(">= ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case GT:
                if (isString) {
                    sb.append("> '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append("> PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append("> ").append(value).append(" ");
                break;
            case GTP:
                sb.append("> ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case INP:
                //Тут ничего не делаем, так как все сделано во FROM
                paramOrder.addInParameter(command.getParamName());
                break;
            case ISNOTNULL:
                sb.append("IS NOT NULL ");
                break;
            case ISNULL:
                sb.append("IS NULL ");
                break;
            case LIKE:
                sb.append("LIKE '").append(likeString).append("' ");
                break;
            case LIKEP:
                sb.append("LIKE ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case LQ:
                if (isString) {
                    sb.append("<= '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append("<= PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append("<= ").append(value).append(" ");
                break;
            case LQP:
                sb.append("<= ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case LT:
                if (isString) {
                    sb.append("< '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append("< PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append("< ").append(value).append(" ");
                break;
            case LTP:
                sb.append("< ? ");
                paramOrder.addParameter(command.getParamName());
                break;
            case NEQ:
                if (isString) {
                    sb.append("<> '").append(value).append("' ");
                    break;
                }
                if (isDate) {
                    sb.append("<> PARSEDATETIME('").append(dateFormat.format(dateValue)).append("', 'yyyy-MM-dd HH:mm:ss.SSS') ");
                    break;
                }
                sb.append("<> ").append(value).append(" ");
                break;
            case NEQP:
                sb.append("<> ? ");
                paramOrder.addParameter(command.getParamName());
                break;
        }

    }



}
