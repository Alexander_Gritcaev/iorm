package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.api.criteria.IORMCommand;
import gas.iorm.core.api.criteria.IORMCondition;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public class IORMCommandImpl<T extends IORMPersistable, V> implements IORMCommand<T, V> {

    private IORMQueryBuilderImpl<T> queryBuilder;

    private Command command;
    private V value;
    private String likeString;
    private Class<V> valueType;
    private String paramName;

    IORMConditionImpl<T> condition;

    public IORMCommandImpl(IORMQueryBuilderImpl<T> queryBuilder, Class<V> valueType) {
        this.queryBuilder = queryBuilder;
        this.valueType = valueType;
    }

    public IORMCondition<T> eq(V value) {
        this.value = value;
        this.command = Command.EQ;
        return returnExpression();
    }

    public IORMCondition<T> gt(V value) {
        this.value = value;
        this.command = Command.GT;
        return returnExpression();
    }

    public IORMCondition<T> gq(V value) {
        this.value = value;
        this.command = Command.GQ;
        return returnExpression();
    }

    public IORMCondition<T> lt(V value) {
        this.value = value;
        this.command = Command.LT;
        return returnExpression();
    }

    public IORMCondition<T> lq(V value) {
        this.value = value;
        this.command = Command.LQ;
        return returnExpression();
    }

    public IORMCondition<T> neq(V value) {
        this.value = value;
        this.command = Command.NEQ;
        return returnExpression();
    }

    public IORMCondition<T> like(String value) {
        this.likeString = value;
        this.command = Command.LIKE;
        return returnExpression();
    }

    public IORMCondition<T> isNull() {
        this.command = Command.ISNULL;
        return returnExpression();
    }

    public IORMCondition<T> isNotNull() {
        this.command = Command.ISNOTNULL;
        return returnExpression();
    }

    public IORMCondition<T> eqp(String paramName) {
        this.command = Command.EQP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> gtp(String paramName) {
        this.command = Command.GTP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> gqp(String paramName) {
        this.command = Command.GQP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> ltp(String paramName) {
        this.command = Command.LTP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> lqp(String paramName) {
        this.command = Command.LQP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> neqp(String paramName) {
        this.command = Command.NEQP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> inp(String paramName) {
        this.command = Command.INP;
        this.paramName = paramName;
        return returnExpression();
    }

    public IORMCondition<T> likep(String paramName) {
        this.command = Command.LIKEP;
        this.paramName = paramName;
        return returnExpression();
    }

    private IORMCondition<T> returnExpression() {
        this.condition = new IORMConditionImpl<T>(this.queryBuilder);
        return this.getCondition();
    }

    Command getCommand() {
        return command;
    }

    V getValue() {
        return value;
    }

    String getLikeString() {
        return likeString;
    }

    IORMConditionImpl<T> getCondition() {
        return condition;
    }

    Class<V> getValueType() {
        return valueType;
    }

    String getParamName() {
        return paramName;
    }

    enum Command {
        EQ, NEQ, GT, GQ, LT, LQ, INP, LIKE, ISNULL, ISNOTNULL,
        EQP, NEQP, GTP, GQP, LTP, LQP, LIKEP;
    }
}
