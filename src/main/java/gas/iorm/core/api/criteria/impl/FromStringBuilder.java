package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.entity.IORMPersistable;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 28.06.16.
 */
public class FromStringBuilder {

    private static Class<? extends IORMPersistable> processField(Class<? extends IORMPersistable> baseClass, String field, IORMFactory factory,
                              List<Class<?>> added, StringBuilder sb) {
        final String[] paths = field.split("\\.");
        Class<?> cls = baseClass;
        Class<? extends IORMPersistable> lastPersistClass = baseClass;
        for (String path : paths) {
            final Class<?> type = factory.getOriginalFieldType(cls, path);
            if (ReflectionFelper.implementsInterface(type, IORMPersistable.class)) {
                lastPersistClass = (Class<? extends IORMPersistable>)type;
            }
            if (!added.contains(type) && ReflectionFelper.implementsInterface(type, IORMPersistable.class)) {
                sb.append(", \"").append(type.getCanonicalName()).append("\".").append(type.getSimpleName()).append(" ");
                added.add(type);

            }
            cls = type;
        }
        return lastPersistClass;
    }

    public static <T extends IORMPersistable> void buildFromSort(final StringBuilder sb, Class<T> baseClass,
                                                         IORMSortImpl<T> sort, IORMFactory factory, List<Class<?>> processedClasses) {
        IORMSortImpl<T> srt = sort;
        while(srt != null && srt.getField() != null) {
            processField(baseClass, srt.getField(), factory, processedClasses, sb);
            srt = srt.getNextOrder();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends IORMPersistable> void buildFromCondition(final StringBuilder sb, Class<T> baseClass,
                                              IORMExpressionImpl<T> expression, IORMFactory factory, List<Class<?>> processedClasses) {
        IORMExpressionImpl<T> exp = expression;
        while (exp != null) {
            if (exp.getExpression() == IORMExpressionImpl.Expression.FIELD) {
                Class<? extends IORMPersistable> lastPersistClass = processField(baseClass, exp.getFieldName(),
                        factory, processedClasses, sb);
                if (exp.getCommand() != null && exp.getCommand().getCondition() != null) {
                    if (exp.getCommand().getCommand() == IORMCommandImpl.Command.INP) {
                        processInCommand(exp, sb, lastPersistClass.getSimpleName());
                    }
                    exp = exp.getCommand().getCondition().getExpression();
                } else {
                    exp = null;
                }
            } else {
                buildFromCondition(sb, baseClass, exp.getComplexExpression(), factory, processedClasses);
                if (exp.getComplexExpression() != null && exp.getCondition() != null) {
                    exp = exp.getCondition().getExpression();
                } else {
                    exp = null;
                }
            }
        }
    }

    private static <T extends IORMPersistable> void processInCommand(IORMExpressionImpl<T> exp, StringBuilder sb, String lastType) {
        //select p.name from Person p join table(id bigint = ?) i on p.id = i.id
        final IORMCommandImpl<T, ?> command = exp.getCommand();
        final String[] fields = exp.getFieldName().split("\\.");
        sb.append(" join table(").append(fields[fields.length - 1]).append(" ")
                .append(command.getValueType().getSimpleName()).append(" =?) ").append(exp.getFieldName().replace(".", "_"))
                .append("_in on ").append(lastType).append(".").append(fields[fields.length - 1])
                .append(" = ").append(exp.getFieldName().replace(".", "_")).append("_in.")
                .append(fields[fields.length - 1]).append(" ");
    }
}
