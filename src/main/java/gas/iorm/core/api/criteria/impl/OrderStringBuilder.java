package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.criteria.IORMSort;
import gas.iorm.core.api.entity.IORMPersistable;

import java.util.List;

/**
 * Created by alex on 12.07.16.
 */
public class OrderStringBuilder {

    public static <T extends IORMPersistable>  void buildOrderString(final StringBuilder sb, Class<?> baseClass,
                                                    IORMSortImpl<T> sort, IORMFactory factory) {
        IORMSortImpl<T> srt = sort;
        while (srt != null && srt.getField() != null) {
            final String field = srt.getField();
            final String[] paths = field.split("\\.");
            Class<?> cls = baseClass;
            boolean isPersistable = false;
            for (String path : paths) {
                final Class<?> type = factory.getOriginalFieldType(cls, path);
                if (ReflectionFelper.implementsInterface(type, IORMPersistable.class)) {
                    cls = type;
                } else {
                    sb.append(cls.getSimpleName()).append(".").append(path).append(" ");
                    if (srt.getOrder() == IORMSort.Order.ASC) {
                        sb.append("ASC, ");
                    } else {
                        sb.append("DESC, ");
                    }
                }
            }
            srt = srt.getNextOrder();
        }
        sb.setLength(sb.length() - 2);
        sb.append(" ");
    }
}
