package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.api.criteria.IORMQuery;
import gas.iorm.core.api.entity.IORMPersistable;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 12.07.16.
 */
public class QueryBuilder {

    public static <T extends IORMPersistable> IORMQuery<T> toQuery(IORMQueryBuilderImpl<T> queryBuilder) {
        final ParamOrder paramOrder = new ParamOrder();
        final String queryString = QueryBuilder.prepareQueryString(queryBuilder, paramOrder);
        final IORMQueryImpl<T> query = new IORMQueryImpl<T>(queryBuilder.getClazz(), queryString, queryBuilder.getFactory(), paramOrder);
        queryBuilder.reset();
        return query;
    }

    private static <T extends IORMPersistable> String prepareQueryString(IORMQueryBuilderImpl<T> queryBuilder, ParamOrder paramOrder) {
        final StringBuilder query = new StringBuilder();
        query.append("FROM ").append(queryBuilder.getClazz().getSimpleName()).append(" ");
        final List<Class<?>> processedClasses = new LinkedList<Class<?>>();
        processedClasses.add(queryBuilder.getClazz());
        FromStringBuilder.buildFromCondition(query, queryBuilder.getClazz(), queryBuilder.getExpression(),
                queryBuilder.getFactory(), processedClasses);
        FromStringBuilder.buildFromSort(query, queryBuilder.getClazz(), queryBuilder.getSort(), queryBuilder.getFactory(),
                processedClasses);
        final StringBuilder whereSort = new StringBuilder();
        final StringBuilder whereCondition = new StringBuilder();
        List<Class<?>> whereProcessed = new LinkedList<Class<?>>();
        if (queryBuilder.getSort() != null) {
            WhereStringBuilder.buildWhereSort(whereSort, queryBuilder.getClazz(), queryBuilder.getSort(),
                    queryBuilder.getFactory(), whereProcessed);
        }
        if (queryBuilder.getExpression() != null && queryBuilder.getExpression().getFieldName() != null) {
            WhereStringBuilder.buildWhereCondition(whereCondition, queryBuilder.getClazz(), queryBuilder.getExpression(),
                    queryBuilder.getFactory(), whereProcessed, paramOrder);
        }
        if (whereSort.length() > 3 || whereCondition.length() > 3) {
            query.append("WHERE ");
            if (whereSort.length() > 3) {
                query.append(whereSort);
            }
            if (whereCondition.length() > 3) {
                if (whereSort.length() > 3) {
                    query.append(" AND ");
                }
                query.append("( ");
                query.append(whereCondition);
                query.append(") ");
            }
        }
        if (queryBuilder.getSort() != null) {
            query.append("ORDER BY ");
            OrderStringBuilder.buildOrderString(query, queryBuilder.getClazz(), queryBuilder.getSort(),
                    queryBuilder.getFactory());
        }
        return query.toString();
    }
}
