package gas.iorm.core.api.criteria.impl;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.criteria.*;
import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public class IORMConditionImpl<T extends IORMPersistable> implements IORMCondition<T> {

    private IORMQueryBuilderImpl<T> queryBuilder;

    private Condition condition;
    private IORMExpressionImpl<T> expression;
//    private IORMExpressionImpl<T> complex;

    public IORMConditionImpl(IORMQueryBuilderImpl<T> queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public IORMExpression<T> and() {
        this.condition = Condition.AND;
        return returnExpression();
    }

    public IORMExpression<T> or() {
        this.condition = Condition.OR;
        return returnExpression();
    }

    public IORMSort<T> orderBy() {
        return this.queryBuilder.orderBy();
    }

//    public IORMExpression<T> complex(IORMExpression<T> complex) {
//        this.complex = (IORMExpressionImpl<T>) complex;
//        this.condition = Condition.COMPLEX;
//        return returnExpression();
//    }

    private IORMExpression<T> returnExpression() {
        this.expression = new IORMExpressionImpl<T>(this.queryBuilder);
        return this.getExpression();
    }

    Condition getCondition() {
        return condition;
    }

    IORMExpressionImpl<T> getExpression() {
        return expression;
    }

//    IORMExpressionImpl<T> getComplex() {
//        return complex;
//    }

    public IORMQuery<T> toQuery() {
        return QueryBuilder.toQuery(this.queryBuilder);
    }


    enum Condition {
        AND, OR
    }
}
