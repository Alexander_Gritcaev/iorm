package gas.iorm.core.api.criteria;

import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public interface IORMCondition<T extends IORMPersistable> {
    IORMExpression<T> and();
    IORMExpression<T> or();
    IORMSort<T> orderBy();
    IORMQuery<T> toQuery();
}
