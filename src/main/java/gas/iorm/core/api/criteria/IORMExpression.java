package gas.iorm.core.api.criteria;

import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 24.06.16.
 */
public interface IORMExpression<T extends IORMPersistable> {
    <V> IORMCommand<T, V> field(String name, Class<V> type);
    IORMCondition<T> complex(IORMExpression<T> complex);
}
