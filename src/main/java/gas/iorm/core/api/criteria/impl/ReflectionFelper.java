package gas.iorm.core.api.criteria.impl;

/**
 * Created by alex on 28.06.16.
 */
public class ReflectionFelper {

    public static boolean implementsInterface(Class<?> clazz, Class<?> in) {
        final Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> inter : interfaces) {
            if (inter.equals(in)) {
                return true;
            }
        }
        final Class<?> superClass = clazz.getSuperclass();
        if (superClass != null && !superClass.equals(Object.class)) {
            return implementsInterface(superClass, in);
        } else {
            return false;
        }
    }
}
