package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
enum SQLCommand {
    LIKE("LIKE"), IN("IN"), IS("IS"), IS_NOT("ISNOT"), GT(">"), GQ(">="), LT("<"), LQ("<="), EQ("="),
    PLUS("+"), MINUS("-"), MULTIPLE("*"), DIVIDE("/");

    private String text;

    SQLCommand(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
