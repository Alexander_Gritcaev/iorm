package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
public class IORMSQLParseException extends Exception {
    public IORMSQLParseException(String message) {
        super(message);
    }
}
