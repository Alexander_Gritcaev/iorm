package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
enum SQLLogic {
    AND("AND"), OR("OR");

    private String text;

    SQLLogic(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
