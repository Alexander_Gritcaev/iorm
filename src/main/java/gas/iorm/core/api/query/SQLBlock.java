package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
enum SQLBlock {
    SELECT("SELECT"), FROM("FROM"), WHERE("WHERE"), ORDER("ORDER"), HAVING("HAVING"), GROUP_BY("GROUPBY");

    private String text;

    SQLBlock(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static SQLBlock findByText(String text) {
        for (SQLBlock block : values()) {
            if (block.getText().hashCode() == text.hashCode() && block.getText().equals(text)) {
                return block;
            }
        }
        return null;
    }

}
