package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
class SQLField {
    private String name;
    private Class clazz;

    public SQLField(String name, Class clazz) {
        this.name = name;
        this.clazz = clazz;
    }

    public String getName() {
        return name;
    }

    public Class getClazz() {
        return clazz;
    }
}
