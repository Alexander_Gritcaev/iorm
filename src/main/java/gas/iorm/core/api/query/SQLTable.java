package gas.iorm.core.api.query;

/**
 * Created by alex on 23.06.16.
 */
class SQLTable {
    private String name;
    private String alias;

    public SQLTable(String name, String alias) {
        this.name = name;
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }
}
