package gas.iorm.core.api.query;

import org.omg.IOP.IOR;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alex on 23.06.16.
 */
public class IORMQueryProcessor {

    private String sourceQuery;

    public IORMQueryProcessor(String sourceQuery) {
        this.sourceQuery = sourceQuery;
    }

    public void analyse() throws IORMSQLParseException {
        final List<Block> blocks = analyseBlocks(sourceQuery);
        for (Block block : blocks) {
            if (block.getBlock() == SQLBlock.FROM) {
                final List<Table> tables = analyseFrom(block.getValue());
            }
        }
    }

    private List<Block> analyseBlocks(String source) {
        StringBuilder pb = new StringBuilder();
        for (SQLBlock block : SQLBlock.values()) {
            pb.append("\\b").append(block.getText()).append("\\b|\\b");
        }
        Pattern pattern = Pattern.compile(pb.substring(0, pb.length() - 3), Pattern.UNICODE_CASE|Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(source);

        final List<Block> blocks = new LinkedList<Block>();
        int startPos = 0;
        while(matcher.find()) {
            if (!blocks.isEmpty()) {
                blocks.get(blocks.size() - 1).setValue(source.substring(startPos, matcher.start()).trim());
            }
            blocks.add(new Block(SQLBlock.findByText(matcher.group())));
            startPos = matcher.end();
        }
        blocks.get(blocks.size() - 1).setValue(source.substring(startPos).trim());
        return blocks;
    }

    private List<Table> analyseFrom(String fromValue) throws IORMSQLParseException {
        final List<Table> result = new LinkedList<Table>();
        final String[] tables = fromValue.split(",");
        for (String tab : tables) {
            String[] names = tab.trim().split(" ");
            if (names.length > 2 || names.length == 0) {
                throw new IORMSQLParseException(tab.trim() + " is not entity name or entity name with alias.");
            }
            //TODO Добавить проверку на наличие сущьности
            result.add(new Table(names[0], (names.length == 2 ? names[1] : names[0])));
        }
        return result;
    }

    private class Block {

        private SQLBlock block;
        private String value;

        public Block(SQLBlock block) {
            this.block = block;
        }

        public SQLBlock getBlock() {
            return block;
        }

        public String getValue() {
            return value;
        }

        public void setBlock(SQLBlock block) {
            this.block = block;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    private class Table {
        private String table;
        private String alias;

        public Table(String table, String alias) {
            this.table = table;
            this.alias = alias;
        }

        public Table() {
        }

        public String getTable() {
            return table;
        }

        public void setTable(String table) {
            this.table = table;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }
    }

}

