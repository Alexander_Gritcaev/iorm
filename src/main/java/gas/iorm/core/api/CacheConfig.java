package gas.iorm.core.api;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMemoryMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.CacheWriteSynchronizationMode;
import org.apache.ignite.cache.affinity.AffinityFunction;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.cache.store.CacheStoreAdapter;

import javax.cache.configuration.Factory;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by alex on 15.07.16.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheConfig {

    StoreConfiguration[] storeConfiguration() default {};
    CacheAtomicityMode atomicMode() default CacheAtomicityMode.TRANSACTIONAL;
    int backups() default 2;
    CacheMode cacheMode() default CacheMode.LOCAL;
    boolean copyOnRead() default true;
    long defaultLockTomeout() default 0;
    boolean eagerTtl() default true;
    boolean invalidate() default false;
    int maxConcurrentAsyncOperations() default 500;
    CacheMemoryMode memoryMode() default CacheMemoryMode.ONHEAP_TIERED;
    long offHeapMaxMemory() default 0;
    CacheWriteSynchronizationMode writeSynchronizationMode() default CacheWriteSynchronizationMode.FULL_SYNC;
    Class<? extends AffinityFunction> affinityFunction() default RendezvousAffinityFunction.class;

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public static @interface  StoreConfiguration {
        Class<? extends Factory<? extends CacheStore<?, ? extends IORMPersistable>>> factory();
        boolean readThrough() default true;
        boolean writeThrough() default true;
        boolean writeBehindEnable() default false;
        int writeBehindFlushSize() default 10240;
        long writeBehindFlushFrequency() default 5000L;
        int writeBehindFlushThreadCount() default 1;
        int writeBehindBatchSize() default 512;
    }
}
