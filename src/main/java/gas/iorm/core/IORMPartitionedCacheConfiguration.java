package gas.iorm.core;

import gas.iorm.core.api.CacheConfig;
import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.configuration.Factory;

/**
 * Created by alex on 17.08.16.
 */
public class IORMPartitionedCacheConfiguration <T extends IORMPartitioned> {

    public void applyAnnotationConfiguration(Class<T> clazz, CacheConfiguration<IORMPartitionKey<T>, T> cacheConfig) {
        final CacheConfig config = clazz.getAnnotation(CacheConfig.class);
        try {
            if (config != null) {
                if (config.cacheMode() != CacheMode.PARTITIONED) {
                    throw new RuntimeException("Entity implements IORMPartitioned must set CacheConfig.cacheMode to PARTITIONED");
                }
                cacheConfig.setAtomicityMode(config.atomicMode())
                        .setBackups(config.backups())
                        .setCacheMode(config.cacheMode())
                        .setCopyOnRead(config.copyOnRead())
                        .setDefaultLockTimeout(config.defaultLockTomeout())
                        .setEagerTtl(config.eagerTtl())
                        .setInvalidate(config.invalidate())
                        .setMaxConcurrentAsyncOperations(config.maxConcurrentAsyncOperations())
                        .setMemoryMode(config.memoryMode())
                        .setOffHeapMaxMemory(config.offHeapMaxMemory())
                        .setWriteSynchronizationMode(config.writeSynchronizationMode())
                        .setAffinity(config.affinityFunction().newInstance());
                applyStorageConfig(config, cacheConfig);
            } else {
                cacheConfig.setCacheMode(CacheMode.PARTITIONED);
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Cant create instance " + config.affinityFunction().getCanonicalName(), e);
        }
    }

    private void applyStorageConfig(CacheConfig config, CacheConfiguration<IORMPartitionKey<T>, T> cacheConfig) {
        if (config.storeConfiguration().length > 0) {
            final CacheConfig.StoreConfiguration sc = config.storeConfiguration()[0];
            try {
                cacheConfig.setCacheStoreFactory((Factory<? extends CacheStore<? super IORMPartitionKey, ? super T>>) sc.factory().newInstance());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            cacheConfig.setReadThrough(sc.readThrough());
            cacheConfig.setWriteThrough(sc.writeThrough());
            cacheConfig.setWriteBehindEnabled(sc.writeBehindEnable());
            cacheConfig.setWriteBehindBatchSize(sc.writeBehindBatchSize());
            cacheConfig.setWriteBehindFlushFrequency(sc.writeBehindFlushFrequency());
            cacheConfig.setWriteBehindFlushThreadCount(sc.writeBehindFlushThreadCount());
            cacheConfig.setWriteBehindFlushSize(sc.writeBehindFlushSize());

        }
    }
}