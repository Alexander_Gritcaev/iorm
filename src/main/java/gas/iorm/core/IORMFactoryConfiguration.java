package gas.iorm.core;

import java.net.URL;
import java.util.Arrays;

/**
 * Created by alex on 04.08.16.
 */
public class IORMFactoryConfiguration {

    private URL defaultIgniteConfigurationURL;
    private String[] classNames = new String[0];
    private String[] classPaths = new String[0];

    public URL getDefaultIgniteConfigurationURL() {
        return defaultIgniteConfigurationURL;
    }

    public void setDefaultIgniteConfigurationURL(URL defaultIgniteConfigurationURL) {
        this.defaultIgniteConfigurationURL = defaultIgniteConfigurationURL;
    }

    public String[] getClassNames() {
        return classNames;
    }

    public void setClassNames(String... classNames) {
        this.classNames = classNames;
    }

    public String[] getClassPaths() {
        return classPaths;
    }

    public void setClassPaths(String... classPaths) {
        this.classPaths = classPaths;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IORMFactory configuration: \n")
                .append("   Class names:\n");
        for (String name : this.classNames) {
            sb.append("       ").append(name).append(";\n");
        }
        sb.append("   Class paths:\n");
        for (String path : classPaths) {
            sb.append("       ").append(path).append(";\n");
        }
        sb.append("   Default Ignite configurationURL :").append(defaultIgniteConfigurationURL).append(".\n");
        return sb.toString();
    }
}
