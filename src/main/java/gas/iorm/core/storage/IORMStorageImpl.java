package gas.iorm.core.storage;

import gas.iorm.core.IORMFactory;
import gas.iorm.core.api.EntityNotFoundException;
import gas.iorm.core.api.criteria.impl.IORMQueryBuilderImpl;
import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 15.06.16.
 */
public class IORMStorageImpl implements IORMStorage {
    private final IORMFactory factory;
    private IgniteTransactions txManager;

    private Map<Class<? extends IORMPersistable>, Method> validateMethods = new HashMap<Class<? extends IORMPersistable>, Method>();

    public IORMStorageImpl(IORMFactory factory, IgniteTransactions txManager) {
        this.factory = factory;
        this.txManager = txManager;
    }

    public <T extends IORMPersistable> T create(Class<T> clazz) {
        return this.factory.getStorage(clazz).create();
    }

    @SuppressWarnings("unchecked")
    public <T extends IORMPersistable> void add(T entity) {
        validate(entity);
        ((IORMCacheStorage<T>)this.factory.getStorage(entity.getClass())).add(entity);
    }

    @SuppressWarnings("unchecked")
    public <T extends IORMPersistable> void addAll(Collection<T> entities) {
        if (entities.isEmpty()) {
            return;
        }
        for (T rec : entities) {
            validate(rec);
        }
        ((IORMCacheStorage<T>)this.factory.getStorage(entities.iterator().next().getClass())).addAll(entities);
    }

    public <T extends IORMPersistable> T get(long id, Class<T> clazz) {
        return this.factory.getStorage(clazz).get(id);
    }

    public <T extends IORMPersistable> Set<T> getAll(Set<Long> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).getAll(ids);
    }

    public <T extends IORMPersistable> Set<T> getAll(Class<T> clazz) {
        return this.factory.getStorage(clazz).getAll();
    }

    @Override
    public <T extends IORMPersistable> boolean remove(long id, Class<T> clazz) {
        return this.factory.getStorage(clazz).remove(id);
    }

    @Override
    public <T extends IORMPersistable> void removeAll(Class<T> clazz) {
        this.factory.getStorage(clazz).removeAll();
    }

    public <T extends IORMPersistable> List<T> executeQuery(Class<T> clazz, String sqlQuery, Object... args) {
        return this.factory.getStorage(clazz).executeQuery(clazz, sqlQuery, args);
    }

    public Transaction startTransaction(TransactionConcurrency concurrency, TransactionIsolation isolation) {
        return this.txManager.txStart(concurrency, isolation);
    }

    public Transaction startTransaction() {
        return this.txManager.txStart(TransactionConcurrency.OPTIMISTIC, TransactionIsolation.READ_COMMITTED);
    }

    public <T extends IORMPersistable> Lock lock(long id, Class<T> clazz) {
        return this.factory.getStorage(clazz).lock(id);
    }

    public <T extends IORMPersistable> Lock lock(Collection<Long> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).lock(ids);
    }

    public <T extends IORMPersistable> void validate(T entity) throws EntityNotFoundException {
        if (!this.validateMethods.containsKey(entity.getClass())) {
            try {
                this.validateMethods.put(entity.getClass(), entity.getClass().getMethod("___validation"));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return;
            }
        }
        final Method validate = this.validateMethods.get(entity.getClass());
        try {
            validate.invoke(entity);
        } catch (IllegalAccessException | InvocationTargetException iae) {
            iae.printStackTrace();
            throw new RuntimeException("Error validating " + entity.getClass().getCanonicalName());
        }
    }

    public <T extends IORMPersistable> boolean containsKey(long id, Class<T> clazz) {
        return this.factory.getStorage(clazz).containsKey(id);
    }

    public <T extends IORMPersistable> boolean containsKey(Set<Long> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).containsKey(ids);
    }

    public <T extends IORMPersistable> IORMQueryBuilderImpl<T> createQuery(Class<T> clazz) {
        return new IORMQueryBuilderImpl<T>(clazz, factory);
    }

    public <T extends IORMPersistable> CacheMetrics getMetrics(Class<T> clazz) {
        return this.factory.getStorage(clazz).getMetrics();
    }

    public <T extends IORMPersistable> CacheMetrics getMetrics(Class<T> clazz, ClusterGroup group) {
        return this.factory.getStorage(clazz).getMetrics(group);
    }

    @Override
    public <T extends IORMPartitioned> T get(IORMPartitionKey<T> id, Class<T> clazz) {
        return this.factory.getStorage(clazz).get(id);
    }

    @Override
    public <T extends IORMPartitioned> Set<T> getPartitionedAll(Set<IORMPartitionKey> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).getPartitionedAll(ids);
    }

    @Override
    public <T extends IORMPartitioned> Lock lock(IORMPartitionKey<T> id, Class<T> clazz) {
        return this.factory.getStorage(clazz).lock(id);
    }

    @Override
    public <T extends IORMPartitioned> Lock lockPartitioned(Collection<IORMPartitionKey> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).lockPartitioned(ids);
    }

    @Override
    public <T extends IORMPartitioned> boolean containsKey(IORMPartitionKey<T> id, Class<T> clazz) {
        return this.factory.getStorage(clazz).containsKey(id);
    }

    @Override
    public <T extends IORMPartitioned> boolean containsPartitionedKey(Set<IORMPartitionKey> ids, Class<T> clazz) {
        return this.factory.getStorage(clazz).containsPartitionedKey(ids);
    }

    @Override
    public <T extends IORMPartitioned> boolean remove(IORMPartitionKey id, Class<T> clazz) {
        return this.factory.getStorage(clazz).remove(id);
    }

    @Override
    public <T extends IORMPersistable> boolean replace(long id, T value, Class<T> clazz) {
        return this.factory.getStorage(clazz).replace(id, value);
    }

    @Override
    public <T extends IORMPersistable> boolean replace(long id, T oldValue, T newValue, Class<T> clazz) {
        return this.factory.getStorage(clazz).replace(id, oldValue, newValue);
    }

    @Override
    public <T extends IORMPersistable> T getAndReplace(long id, T newValue, Class<T> clazz) {
        return this.factory.getStorage(clazz).getAndReplace(id, newValue);
    }

    @Override
    public <T extends IORMPartitioned> boolean replace(IORMPartitionKey id, T value, Class<T> clazz) {
        return this.factory.getStorage(clazz).replace(id, value);
    }

    @Override
    public <T extends IORMPartitioned> boolean replace(IORMPartitionKey id, T oldValue, T newValue, Class<T> clazz) {
        return this.factory.getStorage(clazz).replace(id, oldValue, newValue);
    }

    @Override
    public <T extends IORMPartitioned> T getAndReplace(IORMPartitionKey id, T newValue, Class<T> clazz) {
        return this.factory.getStorage(clazz).getAndReplace(id, newValue);
    }
}
