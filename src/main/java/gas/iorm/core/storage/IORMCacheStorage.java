package gas.iorm.core.storage;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 16.08.16.
 */
public interface IORMCacheStorage<T extends IORMPersistable> {
    T create();

    void add(T entity);

    void addAll(Collection<T> entities);

    T get(long id);

    T get(IORMPartitionKey id);

    Set<T> getAll(Set<Long> ids);

    Map<Long, T> getAllMap(Set<Long> ids);

    Set<T> getPartitionedAll(Set<IORMPartitionKey> ids);

    Map<IORMPartitionKey, T> getPartitionedAllMap(Set<IORMPartitionKey> ids);

    Set<T> getAll();

    List<T> executeQuery(Class<T> clazz, String sqlQuery, Object... args);

    Transaction startTransaction(TransactionConcurrency concurrency, TransactionIsolation isolation)
            throws IllegalStateException;

    Transaction startTransaction() throws IllegalStateException;

    Lock lock(long id);

    Lock lock(IORMPartitionKey id);

    Lock lock(Collection<Long> ids);

    Lock lockPartitioned(Collection<IORMPartitionKey> ids);

    boolean containsKey(long id);

    boolean containsKey(IORMPartitionKey id);

    boolean containsKey(Set<Long> ids);

    boolean containsPartitionedKey(Set<IORMPartitionKey> ids);

    CacheMetrics getMetrics();

    CacheMetrics getMetrics(ClusterGroup group);

    boolean remove(Long id);

    boolean remove(IORMPartitionKey id);

    void removeAll();

    boolean replace(long id, T value);

    boolean replace(long id, T oldValue, T newValue);

    T getAndReplace(long id, T newValue);

    boolean replace(IORMPartitionKey id, T value);

    boolean replace(IORMPartitionKey id, T oldValue, T newValue);

    T getAndReplace(IORMPartitionKey id, T newValue);
}
