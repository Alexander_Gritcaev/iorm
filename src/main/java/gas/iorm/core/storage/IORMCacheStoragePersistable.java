package gas.iorm.core.storage;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import javax.cache.Cache;
import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 10.06.16.
 */
public class IORMCacheStoragePersistable<T extends IORMPersistable> extends IORMCacheStorageBase<T> {

    private final IgniteCache<Long, T> storage;
    private final Class<T> clazz;
    private final IgniteAtomicLong numerator;
    private final IgniteTransactions txManager;

    public IORMCacheStoragePersistable(IgniteCache<Long, T> storage, Class<T> clazz, IgniteAtomicLong numerator,
                                       IgniteTransactions txManager) {
        super(clazz, numerator, txManager, storage);
        this.storage = storage;
        this.clazz = clazz;
        this.numerator = numerator;
        this.txManager = txManager;
    }


    @Override
    public void add(T entity) {
        storage.put(entity.getId(), entity);
    }

    @Override
    public void addAll(Collection<T> entities) {
        final Map<Long, T> map = new HashMap<Long, T>(entities.size());
        for (T rec : entities) {
            map.put(rec.getId(), rec);
        }
        storage.putAll(map);
    }

    @Override
    public T get(long id) {
        return storage.get(id);
    }

    @Override
    public Set<T> getAll(Set<Long> ids) {
        return new HashSet<T>(storage.getAll(ids).values());
    }

    @Override
    public Map<Long, T> getAllMap(Set<Long> ids) {
        return storage.getAll(ids);
    }

    @Override
    public Set<T> getAll() {
        final Set<T> result = new HashSet<T>();
        for (Cache.Entry<Long, T> rec : storage) {
            result.add(rec.getValue());
        }
        return result;
    }

    @Override
    public List<T> executeQuery(Class<T> clazz, String sqlQuery, Object... args) {
        final SqlQuery<Long, T> sql = new SqlQuery<Long, T>(clazz, sqlQuery);
        sql.setArgs(args);
        QueryCursor<Cache.Entry<Long, T>> query = storage.query(sql);
        final List<T> result = new LinkedList<T>();
        for (Cache.Entry<Long, T> ent : query.getAll()) {
            result.add(ent.getValue());
        }
        return result;
    }

    @Override
    public Lock lock(long id) {
         return storage.lock(id);
    }

    @Override
    public Lock lock(Collection<Long> ids) {
        return storage.lockAll(ids);
    }

    @Override
    public boolean containsKey(long id) {
        return storage.containsKey(id);
    }

    @Override
    public boolean containsKey(Set<Long> ids) {
        return storage.containsKeys(ids);
    }

    @Override
    public CacheMetrics getMetrics() {
        return storage.metrics();
    }
    @Override
    public CacheMetrics getMetrics(ClusterGroup group) {
        return  storage.metrics(group);
    }

    @Override
    public boolean remove(Long id) {
        return storage.remove(id);
    }

    @Override
    public void removeAll() {
        storage.removeAll();
    }

    @Override
    public boolean replace(long id, T value) {
        return storage.replace(id, value);
    }

    @Override
    public boolean replace(long id, T oldValue, T newValue) {
        return storage.replace(id, oldValue, newValue);
    }

    @Override
    public T getAndReplace(long id, T newValue) {
        return storage.getAndReplace(id, newValue);
    }

}
