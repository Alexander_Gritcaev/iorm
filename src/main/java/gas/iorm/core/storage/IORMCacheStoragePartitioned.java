package gas.iorm.core.storage;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlQuery;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import javax.cache.Cache;
import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 10.06.16.
 */
public class IORMCacheStoragePartitioned<T extends IORMPartitioned> extends IORMCacheStorageBase<T> {

    private final IgniteCache<IORMPartitionKey<T>, T> storage;
    private final Class<T> clazz;
    private final IgniteAtomicLong numerator;
    private final IgniteTransactions txManager;
    private final IgniteCache<Long, Long> partitionMap;

    public IORMCacheStoragePartitioned(IgniteCache<IORMPartitionKey<T>, T> storage, Class<T> clazz, IgniteAtomicLong numerator,
                                       IgniteTransactions txManager, IgniteCache<Long, Long> partitionMap) {
        super(clazz, numerator, txManager, storage);
        this.storage = storage;
        this.clazz = clazz;
        this.numerator = numerator;
        this.txManager = txManager;
        this.partitionMap = partitionMap;
    }


    @Override
    public void add(T entity) {
        storage.put(new IORMPartitionKey<T>(entity.getId(), entity.getAffinity().getId()), entity);
        partitionMap.put(entity.getId(), entity.getAffinity().getId());
    }

    @Override
    public void addAll(Collection<T> entities) {
        final Map<IORMPartitionKey<T>, T> map = new HashMap<IORMPartitionKey<T>, T>(entities.size());
        final Map<Long, Long> partMap = new HashMap<Long, Long>();
        for (T rec : entities) {
            map.put(new IORMPartitionKey<T>(rec.getId(), rec.getAffinity().getId()), rec);
            partMap.put(rec.getId(), rec.getAffinity().getId());
        }
        storage.putAll(map);
        partitionMap.putAll(partMap);
    }

    @Override
    public T get(long id) {
        return storage.get(new IORMPartitionKey<T>(id, partitionMap.get(id)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get(IORMPartitionKey id) {
        return storage.get((IORMPartitionKey<T>)id);
    }

    @Override
    public Set<T> getAll(Set<Long> ids) {
        final Set<IORMPartitionKey<T>> set = new HashSet<IORMPartitionKey<T>>();
        for (Long id : ids) {
            set.add(new IORMPartitionKey<T>(id, partitionMap.get(id)));
        }
        return new HashSet<T>(storage.getAll(set).values());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Set<T> getPartitionedAll(Set<IORMPartitionKey> ids) {
        return new HashSet<T>(storage.getAll((Set<IORMPartitionKey<T>>)(Object)ids).values());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<IORMPartitionKey, T> getPartitionedAllMap(Set<IORMPartitionKey> ids) {
        return (Map<IORMPartitionKey, T>)(Object)storage.getAll((Set<IORMPartitionKey<T>>)(Object)ids);
    }

    @Override
    public Set<T> getAll() {
        final Set<T> result = new HashSet<T>(storage.size());
        for (Cache.Entry<IORMPartitionKey<T>, T> rec : storage) {
            result.add(rec.getValue());
        }
        return result;
    }

    @Override
    public List<T> executeQuery(Class<T> clazz, String sqlQuery, Object... args) {
        final SqlQuery<IORMPartitionKey<T>, T> sql = new SqlQuery<IORMPartitionKey<T>, T>(clazz, sqlQuery);
        sql.setArgs(args);
        QueryCursor<Cache.Entry<IORMPartitionKey<T>, T>> query = storage.query(sql);
        final List<Cache.Entry<IORMPartitionKey<T>, T>> res = query.getAll();
        final List<T> result = new ArrayList<T>(res.size());
        for (Cache.Entry<IORMPartitionKey<T>, T> ent : res) {
            result.add(ent.getValue());
        }
        return result;
    }

    @Override
    public Lock lock(long id) {
         return storage.lock(new IORMPartitionKey<T>(id, partitionMap.get((id))));
    }

    @SuppressWarnings("unchecked")
    @Override
    public Lock lock(IORMPartitionKey id) {
        return storage.lock(id);
    }

    @Override
    public Lock lock(Collection<Long> ids) {
        final Set<IORMPartitionKey<T>> set = new HashSet<IORMPartitionKey<T>>(ids.size());
        for (Long id : ids) {
            set.add(new IORMPartitionKey<T>(id, partitionMap.get(id)));
        }
        return storage.lockAll(set);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Lock lockPartitioned(Collection<IORMPartitionKey> ids) {
        return storage.lockAll((Collection<IORMPartitionKey<T>>) (Object)ids);
    }

    @Override
    public boolean containsKey(long id) {
        return storage.containsKey(new IORMPartitionKey<T>(id, partitionMap.get(id)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean containsKey(IORMPartitionKey id) {
        return storage.containsKey(id);
    }

    @Override
    public boolean containsKey(Set<Long> ids) {
        final Set<IORMPartitionKey<T>> set = new HashSet<IORMPartitionKey<T>>(ids.size());
        for (Long id : ids) {
            set.add(new IORMPartitionKey<T>(id, partitionMap.get(id)));
        }
        return storage.containsKeys(set);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean containsPartitionedKey(Set<IORMPartitionKey> ids) {
        return storage.containsKeys((Set<IORMPartitionKey<T>>)(Object)ids);
    }

    @Override
    public boolean remove(Long id) {
        final IORMPartitionKey<T> key = new IORMPartitionKey<>(id, partitionMap.get(id));
        final T rec = storage.get(key);
        final boolean res = storage.remove(key);
        boolean partMapRemove = false;
        if (res) {
            partMapRemove = partitionMap.remove(key.getPartitionId());
        }
        if (!res) {
            return false;
        }
        if (!partMapRemove) {
            storage.put(key, rec);
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(IORMPartitionKey id) {
        final T rec = storage.get((IORMPartitionKey<T>)id);
        final boolean res = storage.remove(id);
        boolean partMapRemove = false;
        if (res) {
            partMapRemove = partitionMap.remove(id.getPartitionId());
        }
        if (!res) {
            return false;
        }
        if (!partMapRemove) {
            storage.put(id, rec);
            return false;
        }
        return true;
    }

    @Override
    public void removeAll() {
        final Set<Long> set = new HashSet<Long>();
        for (Cache.Entry<IORMPartitionKey<T>, T> entry : storage) {
            set.add(entry.getKey().getId());
        }
        storage.removeAll();
        partitionMap.removeAll(set);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean replace(IORMPartitionKey id, T value) {
        return storage.replace(id, value);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean replace(IORMPartitionKey id, T oldValue, T newValue) {
        return storage.replace(id, oldValue, newValue);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getAndReplace(IORMPartitionKey id, T newValue) {
        return (T)storage.getAndReplace(id, newValue);
    }
}
