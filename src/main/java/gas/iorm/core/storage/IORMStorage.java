package gas.iorm.core.storage;

import gas.iorm.core.api.EntityNotFoundException;
import gas.iorm.core.api.criteria.impl.IORMQueryBuilderImpl;
import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.cluster.ClusterMetrics;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import java.util.*;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 15.06.16.
 */
public interface IORMStorage {
    <T extends IORMPersistable> T create(Class<T> clazz);
    <T extends IORMPersistable> void add(T entity);
    <T extends IORMPersistable> void addAll(Collection<T> entities);
    <T extends IORMPersistable> T get(long id, Class<T> clazz);
    <T extends IORMPartitioned> T get(IORMPartitionKey<T> id, Class<T> clazz);
    <T extends IORMPersistable> Set<T> getAll(Set<Long> ids, Class<T> clazz);
    <T extends IORMPartitioned> Set<T> getPartitionedAll(Set<IORMPartitionKey> ids, Class<T> clazz);
    <T extends IORMPersistable> Set<T> getAll(Class<T> clazz);
    <T extends IORMPersistable> List<T> executeQuery(Class<T> entityClass, String sqlQuery, Object... args);
    <T extends IORMPersistable> void removeAll(Class<T> clazz);
    Transaction startTransaction(TransactionConcurrency concurrency, TransactionIsolation isolation);
    Transaction startTransaction();
    <T extends IORMPersistable> Lock lock(long id, Class<T> clazz);
    <T extends IORMPartitioned> Lock lock(IORMPartitionKey<T> id, Class<T> clazz);
    <T extends IORMPersistable> Lock lock(Collection<Long> ids, Class<T> clazz);
    <T extends IORMPartitioned> Lock lockPartitioned(Collection<IORMPartitionKey> ids, Class<T> clazz);
    <T extends IORMPersistable> void validate(T entity) throws EntityNotFoundException;
    <T extends IORMPersistable> boolean containsKey(long id, Class<T> clazz);
    <T extends IORMPartitioned> boolean containsKey(IORMPartitionKey<T> id, Class<T> clazz);
    <T extends IORMPersistable> boolean containsKey(Set<Long> ids, Class<T> clazz);
    <T extends IORMPartitioned> boolean containsPartitionedKey(Set<IORMPartitionKey> ids, Class<T> clazz);
    <T extends IORMPersistable> IORMQueryBuilderImpl<T> createQuery(Class<T> clazz);

    <T extends IORMPersistable> CacheMetrics getMetrics(Class<T> clazz);
    <T extends IORMPersistable> CacheMetrics getMetrics(Class<T> clazz, ClusterGroup group);

    <T extends IORMPersistable> boolean remove(long id, Class<T> clazz);
    <T extends IORMPartitioned> boolean remove(IORMPartitionKey id, Class<T> clazz);

    <T extends IORMPersistable> boolean replace(long id, T value,Class<T> clazz);
    <T extends IORMPersistable> boolean replace(long id, T oldValue, T newValue, Class<T> clazz);
    <T extends IORMPersistable> T getAndReplace(long id, T newValue, Class<T> clazz);
    <T extends IORMPartitioned> boolean replace(IORMPartitionKey id, T value, Class<T> clazz);
    <T extends IORMPartitioned> boolean replace(IORMPartitionKey id, T oldValue, T newValue, Class<T> clazz);
    <T extends IORMPartitioned> T getAndReplace(IORMPartitionKey id, T newValue, Class<T> clazz);

}
