package gas.iorm.core.storage;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;

/**
 * Created by alex on 10.08.16.
 */
public class IORMKeyGenerator {

    public static <T extends IORMPartitioned> IORMPartitionKey<T> generateAfinityKey(T record) {
        return new IORMPartitionKey<>(record.getId(), record.getAffinity().getId());
    }

}
