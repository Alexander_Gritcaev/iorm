package gas.iorm.core.storage;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.api.entity.IORMPartitioned;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteTransactions;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;

/**
 * Created by alex on 16.08.16.
 */
public abstract class IORMCacheStorageBase <T extends IORMPersistable> implements IORMCacheStorage<T> {

    private final Class<T> clazz;
    private final IgniteAtomicLong numerator;
    private final IgniteTransactions txManager;
    private final IgniteCache<?, T> storage;

    public IORMCacheStorageBase(Class<T> clazz, IgniteAtomicLong numerator, IgniteTransactions txManager, IgniteCache<?, T> storage) {
        this.clazz = clazz;
        this.numerator = numerator;
        this.txManager = txManager;
        this.storage = storage;
    }

    @Override
    public T create() {
        try {
            T rec = clazz.newInstance();
            rec.setId(numerator.getAndIncrement());
            return rec;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Transaction startTransaction(TransactionConcurrency concurrency, TransactionIsolation isolation)
            throws IllegalStateException {
        return txManager.txStart(concurrency, isolation);
    }

    @Override
    public Transaction startTransaction() throws IllegalStateException {
        return txManager.txStart(TransactionConcurrency.OPTIMISTIC, TransactionIsolation.READ_COMMITTED);
    }

    @Override
    public CacheMetrics getMetrics() {
        return storage.metrics();
    }

    @Override
    public CacheMetrics getMetrics(ClusterGroup group) {
        return  storage.metrics(group);
    }

    @Override
    public void add(T entity) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public void addAll(Collection<T> entities) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public T get(long id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public T get(IORMPartitionKey id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Set<T> getAll(Set<Long> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Set<T> getPartitionedAll(Set<IORMPartitionKey> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Set<T> getAll() {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public List<T> executeQuery(Class<T> clazz, String sqlQuery, Object... args) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Lock lock(long id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Lock lock(IORMPartitionKey id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Lock lock(Collection<Long> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Lock lockPartitioned(Collection<IORMPartitionKey> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean containsKey(long id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean containsKey(IORMPartitionKey id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean containsKey(Set<Long> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean containsPartitionedKey(Set<IORMPartitionKey> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean remove(Long id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean remove(IORMPartitionKey id) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public void removeAll() {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean replace(long id, T value) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean replace(long id, T oldValue, T newValue) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public T getAndReplace(long id, T newValue) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean replace(IORMPartitionKey id, T value) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public boolean replace(IORMPartitionKey id, T oldValue, T newValue) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public T getAndReplace(IORMPartitionKey id, T newValue) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Map<Long, T> getAllMap(Set<Long> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }

    @Override
    public Map<IORMPartitionKey, T> getPartitionedAllMap(Set<IORMPartitionKey> ids) {
        throw new UnsupportedOperationException("This operation is not implemented in this type storage");
    }
}
