package gas.iorm.core;

import gas.iorm.core.api.CacheConfig;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.affinity.rendezvous.RendezvousAffinityFunction;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.configuration.CacheConfiguration;

import javax.cache.configuration.Factory;


/**
 * Created by alex on 15.07.16.
 */
public class IORMCacheConfiguration<T extends IORMPersistable> {

    public void applyAnnotationConfiguration(Class<T> clazz, CacheConfiguration<Long, T> cacheConfig) {
        final CacheConfig config = clazz.getAnnotation(CacheConfig.class);
        try {
            if (config != null) {
                cacheConfig.setAtomicityMode(config.atomicMode())
                        .setBackups(config.backups())
                        .setCacheMode(config.cacheMode())
                        .setCopyOnRead(config.copyOnRead())
                        .setDefaultLockTimeout(config.defaultLockTomeout())
                        .setEagerTtl(config.eagerTtl())
                        .setInvalidate(config.invalidate())
                        .setMaxConcurrentAsyncOperations(config.maxConcurrentAsyncOperations())
                        .setMemoryMode(config.memoryMode())
                        .setOffHeapMaxMemory(config.offHeapMaxMemory())
                        .setWriteSynchronizationMode(config.writeSynchronizationMode())
                        .setAffinity(config.affinityFunction().newInstance());


                applyStorageConfig(config, cacheConfig);
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Cant create instance " + config.affinityFunction().getCanonicalName(), e);
        }
    }

    private void applyStorageConfig(CacheConfig config, CacheConfiguration<Long, T> cacheConfig) {
        if (config.storeConfiguration().length > 0) {
            final CacheConfig.StoreConfiguration sc = config.storeConfiguration()[0];
            try {
                cacheConfig.setCacheStoreFactory((Factory<? extends CacheStore<? super Long, ? super T>>) sc.factory().newInstance());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            cacheConfig.setReadThrough(sc.readThrough());
            cacheConfig.setWriteThrough(sc.writeThrough());
            cacheConfig.setWriteBehindEnabled(sc.writeBehindEnable());
            cacheConfig.setWriteBehindBatchSize(sc.writeBehindBatchSize());
            cacheConfig.setWriteBehindFlushFrequency(sc.writeBehindFlushFrequency());
            cacheConfig.setWriteBehindFlushThreadCount(sc.writeBehindFlushThreadCount());
            cacheConfig.setWriteBehindFlushSize(sc.writeBehindFlushSize());

        }
    }

}
