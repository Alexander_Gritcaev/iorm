package gas.iorm.core;

import gas.iorm.core.entity.PersistEntityInner;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.lang.IgniteBiInClosure;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.sql.*;
import java.util.Collection;
import java.util.Map;

/**
 * Created by alex on 15.07.16.
 */
public class InnerPersistenceAdapter extends BaseAdapter implements CacheStore<Long, PersistEntityInner> {

    public PersistEntityInner load(Long aLong) throws CacheLoaderException {
        //All entities must be loaded at startup
        System.out.println("Load method invoked");
        return null;
    }

    public Map<Long, PersistEntityInner> loadAll(Iterable<? extends Long> iterable) throws CacheLoaderException {
        //All entries must be loaded at startup
        System.out.println("LoadAll method invoked");
        return null;
    }

    public void write(Cache.Entry<? extends Long, ? extends PersistEntityInner> entry) throws CacheWriterException {
        System.out.println("Write method invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement insert = conn.prepareStatement("INSERT INTO persist_entity_inner (id, inner_name, inner_amount) VALUES (?, ?, ?)");
            final PreparedStatement update = conn.prepareStatement("UPDATE persist_entity_inner SET inner_name = ?, inner_amount = ? WHERE id = ?");
            final PersistEntityInner rec = entry.getValue();
            update.setString(1, rec.getInnerName());
            update.setBigDecimal(2, rec.getInnerAmount());
            update.setLong(3, rec.getId());
            if (update.executeUpdate() == 0) {
                insert.setLong(1, rec.getId());
                insert.setString(2, rec.getInnerName());
                insert.setBigDecimal(3, rec.getInnerAmount());
                insert.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error saving entry.", e);
        }

    }

    public void writeAll(Collection<Cache.Entry<? extends Long, ? extends PersistEntityInner>> collection) throws CacheWriterException {
        System.out.println("WriteAll method invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement insert = conn.prepareStatement("INSERT INTO persist_entity_inner (id, inner_name, inner_amount) VALUES (?, ?, ?)");
            final PreparedStatement update = conn.prepareStatement("UPDATE persist_entity_inner SET inner_name = ?, inner_amount = ? WHERE id = ?");
            for(Cache.Entry<? extends Long, ? extends PersistEntityInner> entity : collection) {
                final PersistEntityInner rec = entity.getValue();
                update.setString(1, rec.getInnerName());
                update.setBigDecimal(2, rec.getInnerAmount());
                update.setLong(3, rec.getId());
                if (update.executeUpdate() == 0) {
                    insert.setLong(1, rec.getId());
                    insert.setString(2, rec.getInnerName());
                    insert.setBigDecimal(3, rec.getInnerAmount());
                    insert.executeUpdate();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error saving entries.", e);
        }
    }

    public void delete(Object o) throws CacheWriterException {
        System.out.println("Delete invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement delete = conn.prepareStatement("DELETE FROM persist_entity_inner WHERE id = ?");
            delete.setLong(1, (Long)o);
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException(e);
        }
    }

    public void deleteAll(Collection<?> collection) throws CacheWriterException {
        System.out.println("Delete all invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement delete = conn.prepareStatement("DELETE FROM persist_entity_inner WHERE id = ?");
            for (Object o : collection) {
                delete.setLong(1, (Long) o);
                delete.addBatch();
            }
            delete.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException(e);
        }
    }

    public void loadCache(IgniteBiInClosure<Long, PersistEntityInner> clo, Object... args) {
        System.out.println("Load cache invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement select = conn.prepareStatement("SELECT id, inner_name, inner_amount FROM persist_entity_inner;");
            final ResultSet result = select.executeQuery();
            while (result.next()) {
                final PersistEntityInner rec = new PersistEntityInner();
                rec.setId(result.getLong(1));
                rec.setInnerName(result.getString(2));
                rec.setInnerAmount(result.getBigDecimal(3));
                clo.apply(rec.getId(), rec);
            }
            result.close();
            select.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheLoaderException("Error loading cache", e);
        }
    }

}
