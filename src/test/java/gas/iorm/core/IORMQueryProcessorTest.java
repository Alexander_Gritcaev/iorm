package gas.iorm.core;

import gas.iorm.core.api.query.IORMQueryProcessor;
import org.junit.Test;

/**
 * Created by alex on 23.06.16.
 */
public class IORMQueryProcessorTest {

    @Test
    public void test() throws Exception {
        IORMQueryProcessor processor = new IORMQueryProcessor("FROM CacheEntity e, User WHERE e.createdBy.name = 'Test name'");
        processor.analyse();
    }

}
