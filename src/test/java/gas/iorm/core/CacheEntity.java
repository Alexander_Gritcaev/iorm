package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.List;

/**
 * Created by alex on 10.06.16.
 */
public class CacheEntity extends Parent implements IORMPersistable, Serializable {

    @QuerySqlField(index = true)
    private String label;

    @QuerySqlField
    private User createdBy;

    private List<User> userList;


    private transient String trans;

    public CacheEntity() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
