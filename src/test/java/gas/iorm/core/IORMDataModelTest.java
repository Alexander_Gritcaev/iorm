package gas.iorm.core;

import gas.iorm.core.api.criteria.impl.IORMQueryBuilderImpl;
import gas.iorm.core.storage.IORMStorage;
import org.junit.Test;

/**
 * Created by alex on 28.06.16.
 */
public class IORMDataModelTest {

    @Test
    public void test() throws Exception {
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setClassNames("gas.iorm.core.CacheEntity");
        IORMFactory factory = IORMFactory.getInstance(config);
        IORMStorage storage = factory.getStorage();
        IORMQueryBuilderImpl<CacheEntity> builder = storage.createQuery(CacheEntity.class);
        builder.where().field("createdBy.name", String.class).eq("Test name").and().field("label", String.class).like("%1%");

    }
}
