package gas.iorm.core.entity;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QueryGroupIndex;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by alex on 01.07.16.
 */
public class QueryEntityParent implements IORMPersistable, Serializable {

    @QuerySqlField(index = true)
    private long id;

    @QuerySqlField(orderedGroups = {@QuerySqlField.Group(name = "ind_group", order = 0)})
    private Date startDate;

    @QuerySqlField(orderedGroups = {@QuerySqlField.Group(name = "ind_group", order = 1)})
    private Date endDate;

    @QuerySqlField(index = true)
    private QueryEntityInner parentInner;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public QueryEntityInner getParentInner() {
        return parentInner;
    }

    public void setParentInner(QueryEntityInner parentInner) {
        this.parentInner = parentInner;
    }
}
