package gas.iorm.core.entity;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QueryGroupIndex;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Created by alex on 01.07.16.
 */

public class QueryEntityInner implements IORMPersistable, Serializable {

    @QuerySqlField(index = true)
    private long id;

    @QuerySqlField(index = true)
    private String name;

    @QuerySqlField(index = true)
    private QuerySecond second;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QuerySecond getSecond() {
        return second;
    }

    public void setSecond(QuerySecond second) {
        this.second = second;
    }
}
