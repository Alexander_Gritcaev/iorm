package gas.iorm.core.entity;

import gas.iorm.core.InnerStorageFactory;
import gas.iorm.core.StorageFactory;
import gas.iorm.core.api.CacheConfig;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;

/**
 * Created by alex on 25.07.16.
 */
@CacheConfig(atomicMode = CacheAtomicityMode.TRANSACTIONAL, cacheMode = CacheMode.PARTITIONED,
        copyOnRead = true,
        storeConfiguration = @CacheConfig.StoreConfiguration(factory = StorageFactory.class,
                readThrough = false, writeThrough = true))
public class PersistEntity implements IORMPersistable {
    private long id;
    private String name;
    private PersistEntityInner inner;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PersistEntityInner getInner() {
        return inner;
    }

    public void setInner(PersistEntityInner inner) {
        this.inner = inner;
    }
}
