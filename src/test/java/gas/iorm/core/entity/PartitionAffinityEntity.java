package gas.iorm.core.entity;

import gas.iorm.core.api.entity.IORMPartitioned;

/**
 * Created by alex on 10.08.16.
 */
public class PartitionAffinityEntity implements IORMPartitioned<PartitionMainEntity> {
    private long id;
    private String affinityName;
    private PartitionMainEntity parent;

    @Override
    public PartitionMainEntity getAffinity() {
        return this.getParent();
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getAffinityName() {
        return affinityName;
    }

    public void setAffinityName(String affinityName) {
        this.affinityName = affinityName;
    }

    public PartitionMainEntity getParent() {
        return parent;
    }

    public void setParent(PartitionMainEntity parent) {
        this.parent = parent;
    }
}
