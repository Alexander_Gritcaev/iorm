package gas.iorm.core.entity;

import gas.iorm.core.InnerStorageFactory;
import gas.iorm.core.api.CacheConfig;
import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;

import java.math.BigDecimal;

/**
 * Created by alex on 25.07.16.
 */
@CacheConfig(atomicMode = CacheAtomicityMode.TRANSACTIONAL, cacheMode = CacheMode.PARTITIONED,
        copyOnRead = true,
        storeConfiguration = @CacheConfig.StoreConfiguration(factory = InnerStorageFactory.class,
                readThrough = false, writeThrough = true))
public class PersistEntityInner implements IORMPersistable {
    private long id;
    private String innerName;
    private BigDecimal innerAmount;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public BigDecimal getInnerAmount() {
        return innerAmount;
    }

    public void setInnerAmount(BigDecimal innerAmount) {
        this.innerAmount = innerAmount;
    }
}
