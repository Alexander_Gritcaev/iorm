package gas.iorm.core.entity;

import gas.iorm.core.api.entity.IORMPersistable;

import java.util.List;

/**
 * Created by alex on 10.08.16.
 */
public class PartitionMainEntity implements IORMPersistable {
    private long id;
    private String name;
    private PartitionAffinityEntity affinity;
    private List<PartitionAffinityEntity> partitionedList;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PartitionAffinityEntity getAffinity() {
        return affinity;
    }

    public void setAffinity(PartitionAffinityEntity affinity) {
        this.affinity = affinity;
    }

    public List<PartitionAffinityEntity> getPartitionedList() {
        return partitionedList;
    }

    public void setPartitionedList(List<PartitionAffinityEntity> partitionedList) {
        this.partitionedList = partitionedList;
    }
}
