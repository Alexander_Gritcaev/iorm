package gas.iorm.core.entity;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;

/**
 * Created by alex on 01.07.16.
 */
public class QuerySecond implements IORMPersistable, Serializable {

    @QuerySqlField
    private long id;

    @QuerySqlField(index = true)
    private String secondName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
