package gas.iorm.core.entity;

import org.apache.ignite.cache.query.annotations.QueryGroupIndex;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alex on 01.07.16.
 */
@QueryGroupIndex(name = "ind_group")
public class QueryEntity extends QueryEntityParent implements Serializable {

    @QuerySqlField(orderedGroups = {@QuerySqlField.Group(name="ind_group", order = 0)})
    private double amount;

    private List<QuerySecond> secondList;


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<QuerySecond> getSecondList() {
        return secondList;
    }

    public void setSecondList(List<QuerySecond> secondList) {
        this.secondList = secondList;
    }
}
