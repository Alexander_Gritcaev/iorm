package gas.iorm.core;

import gas.iorm.core.entity.PersistEntity;
import gas.iorm.core.entity.PersistEntityInner;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.transactions.Transaction;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alex on 26.07.16.
 */
public class IORMPersistanceTest extends Assert {

    @Test
    public void test() throws Exception {
        URL configuration = getClass().getClassLoader().getResource("ignite-default.xml");
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setDefaultIgniteConfigurationURL(configuration);
        config.setClassNames("gas.iorm.core.entity.PersistEntityInner","gas.iorm.core.entity.PersistEntity");
        final IORMFactory factory = IORMFactory.getInstance(config);
        final IORMStorage storage = factory.getStorage();

        Set<PersistEntity> map = storage.<PersistEntity>getAll(PersistEntity.class);
        if (map.isEmpty()) {
            long point1 = System.currentTimeMillis();
            final Transaction t = storage.startTransaction();
            for (long l = 0; l < 10000; l++) {
                final PersistEntity rec = storage.create(PersistEntity.class);
                rec.setName("Persist entity, random number "  + Double.toString(Math.random() * 10000.0));
                final PersistEntityInner irec = storage.create(PersistEntityInner.class);
                irec.setInnerName("Inner entry " + Long.toString(l));
                irec.setInnerAmount(new BigDecimal(Math.random() * 10000.0));
                storage.add(irec);
                rec.setInner(irec);
                storage.add(rec);
            }
            t.commit();
            long point2 = System.currentTimeMillis();
            System.out.println("Records inserted in " + (point2 - point1) + " mills");
        } else {
            PersistEntity rec = storage.create(PersistEntity.class);
            assert (rec.getId() == -9223372036854755807L);
            long point1 = System.currentTimeMillis();
            storage.removeAll(PersistEntity.class);
            long point2 = System.currentTimeMillis();
            System.out.println("Records removed in " + (point2 - point1) + " mills");
        }
    }
}
