package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPartitionKey;
import gas.iorm.core.entity.PartitionAffinityEntity;
import gas.iorm.core.entity.PartitionMainEntity;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.transactions.Transaction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 24.08.16.
 */
public class IORMPartitionedEntityTest {

    @Test
    public void test() throws Exception {
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setClassNames("gas.iorm.core.entity.PartitionMainEntity",
                "gas.iorm.core.entity.PartitionAffinityEntity");
        final IORMFactory factory = IORMFactory.getInstance(config);
        final IORMStorage storage = factory.getStorage();

        Transaction t = storage.startTransaction();

        PartitionMainEntity me = storage.create(PartitionMainEntity.class);
        me.setName("Main entry test name");
        storage.add(me);
        PartitionAffinityEntity ae = storage.create(PartitionAffinityEntity.class);
        ae.setAffinityName("Affinity test name field");
        ae.setParent(me);
        storage.add(ae);
        me.setAffinity(ae);
        storage.add(ae);
        storage.add(me);

        t.commit();

        PartitionMainEntity rme = storage.get(me.getId(), PartitionMainEntity.class);
        PartitionAffinityEntity rae = storage.get(ae.getId(), PartitionAffinityEntity.class);

        PartitionAffinityEntity rae2 = storage.get(new IORMPartitionKey<PartitionAffinityEntity>(ae.getId(), ae.getAffinity().getId()), PartitionAffinityEntity.class);

        assert rme != null;
        assert rae != null;
        assert rae2 != null;

        Transaction tt = storage.startTransaction();
        List<PartitionAffinityEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            PartitionAffinityEntity e = storage.create(PartitionAffinityEntity.class);
            e.setAffinityName("Affiniti name " + Math.random() * 100000.0);
            e.setParent(rme);
            storage.add(e);
            list.add(e);
        }
        rme.setPartitionedList(list);
        storage.add(rme);

        tt.commit();

        PartitionMainEntity rrme = storage.get(rme.getId(), PartitionMainEntity.class);

        assert rrme.getPartitionedList().size() == 10;

        Transaction ttt = storage.startTransaction();
        PartitionAffinityEntity ne = storage.create(PartitionAffinityEntity.class);
        ne.setAffinityName("New entity");
        ne.setParent(rrme);
        storage.add(ne);

        rrme.getPartitionedList().add(ne);
        storage.add(rrme);

        ttt.commit();

        PartitionMainEntity rrme2 = storage.get(rme.getId(), PartitionMainEntity.class);
        assert rrme2.getPartitionedList().size() == 11;

        factory.stop();
    }

}
