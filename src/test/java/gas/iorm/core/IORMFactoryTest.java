package gas.iorm.core;

import gas.iorm.core.api.criteria.impl.IORMQueryBuilderImpl;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.transactions.Transaction;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 10.06.16.
 */
public class IORMFactoryTest {

    @Test
    public void test() throws Exception {
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setClassNames("gas.iorm.core.User", "gas.iorm.core.CacheEntity");
        final IORMFactory factory = IORMFactory.getInstance(config);

        IORMStorage storage = factory.getStorage();

//        Transaction transaction = storage.startTransaction();
//        CacheEntity test = storage.create(CacheEntity.class);
//        test.setLabel("Validation test");
//        User validUser = storage.create(User.class);
//        validUser.setName("Valid user name");
//        test.setCreatedBy(validUser);
//        storage.add(validUser);
//        User validParentUser = storage.create(User.class);
//        validParentUser.setName("Parent validation test User");
//        test.setParentUser(validParentUser);
//        storage.add(validParentUser);
//
//        User listUser1 = storage.create(User.class);
//        User listUser2 = storage.create(User.class);
//        test.setUserList(new ArrayList<User>());
//        test.getUserList().add(listUser1);
//        test.getUserList().add(listUser2);
//        storage.add(listUser1);
//        storage.add(listUser2);
//        storage.add(test);
//        transaction.commit();


        long point0 = System.currentTimeMillis();


        Transaction tx = storage.startTransaction();
        for (long l = 0; l < 1000; l++) {
            final CacheEntity ent = storage.create(CacheEntity.class);
            ent.setLabel("Строка ИД " + Long.toString(ent.getId()));
            ent.setTrans("Трансиент поле");
            for (int i = 0; i < 6; i++) {
                final User user = storage.create(User.class);
                user.setName("Пользователь " + l + " -- " + i);
                user.setPass("Пароль пользователя " + l + " - " + i);
                ent.getUserList().add(user);
                storage.add(user);
            }
            User creationUser = storage.create(User.class);
            creationUser.setName("Создавший пользователь для " + l);
            ent.setCreatedBy(creationUser);
            storage.add(creationUser);
            storage.add(ent);
        }
        tx.commit();

        List<CacheEntity> sql = storage.executeQuery(CacheEntity.class, "from CacheEntity, \"gas.iorm.core.User\".User where CacheEntity.createdBy = User.id AND User.name LIKE ?",
                "Создавший пользователь для 5%");
        System.out.println("Sql size is " + sql.size());
        assert(sql.size() == 111);

        long point1 = System.currentTimeMillis();
        System.out.println("Records inserted in " + (point1 - point0) + " mils");
        List<CacheEntity> res = storage.executeQuery(CacheEntity.class, "from CacheEntity where CacheEntity.label LIKE ? ORDER BY CacheEntity.id", "Строка ИД -9223372036854775%");

        long point2 = System.currentTimeMillis();
        System.out.println("Select executed in " + (point2 - point1) + " mils. Count = " + res.size());

        tx = storage.startTransaction();
        CacheEntity e = res.get(0);
        User u = storage.create(User.class);
        u.setName("Добавленный пользователь");
        e.getUserList().add(u);
        storage.add(u);
        storage.add(e);

        tx.commit();

        final List<Long> args = new LinkedList<Long>();
        for (long i = 100; i < 200; i++) {
            args.add(Long.MIN_VALUE + i);
        }
        List<CacheEntity> resList = storage.executeQuery(CacheEntity.class, "from CacheEntity where CacheEntity.id IS NOT NULL ORDER BY CacheEntity.id");

        List<CacheEntity> res2 = storage.executeQuery(CacheEntity.class, "from CacheEntity where CacheEntity.label LIKE ? ORDER BY CacheEntity.id", "Строка ИД -9223372036854775%");

        IORMQueryBuilderImpl<CacheEntity> queryBuilder = storage.createQuery(CacheEntity.class);
        List<CacheEntity> queryRes = queryBuilder
                .where().field("createdBy.name", String.class).likep("name")
                .toQuery().setParameter("name", "Создавший пользователь для 5%")
                .execute();
        assert(queryRes.size() == 111);

//        IORMQueryBuilder<CacheEntity> qb2 = storage.createQuery(CacheEntity.class);
//        IORMExpression<CacheEntity> complex = qb2.complex();
//        complex.field("label", String.class).like("?").and().field("id", String.class).gt("?");
//
//        IORMQuery<CacheEntity> q2 = qb2.where().field("createdBy.name", String.class).like("?")
//                .or().complex(complex).or().field("id", Long.class).eq(Long.MIN_VALUE + 10L).toQuery()
//                .setParameters("Создавший пользователь для 5%", "Строка ИД%" , Long.MIN_VALUE);
//        List<CacheEntity> queryRes2 = q2.execute();

        tx = storage.startTransaction();
        User uu = res2.get(0).getUserList().get(1);
        uu.setName(uu.getName() + " Измененный");
        storage.add(uu);
        tx.commit();

        List<CacheEntity> res3 = storage.executeQuery(CacheEntity.class, "from CacheEntity where CacheEntity.label LIKE ? ORDER BY CacheEntity.id", "Строка ИД -9223372036854775%");

        factory.stop();
    }
}
