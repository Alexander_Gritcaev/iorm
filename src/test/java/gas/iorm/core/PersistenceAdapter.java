package gas.iorm.core;

import gas.iorm.core.entity.PersistEntity;
import gas.iorm.core.entity.PersistEntityInner;
import org.apache.ignite.cache.store.CacheStore;
import org.apache.ignite.lang.IgniteBiInClosure;
import org.jetbrains.annotations.Nullable;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by alex on 27.07.16.
 */
public class PersistenceAdapter extends BaseAdapter implements CacheStore<Long, PersistEntity> {

    @Override
    public void loadCache(IgniteBiInClosure<Long, PersistEntity> clo, @Nullable Object... args) throws CacheLoaderException {
        try {
            final Connection conn = getConnection();
            final PreparedStatement select = conn.prepareStatement("SELECT id, name, persist_entity_inner_id FROM public.persist_entity");
            final ResultSet result = select.executeQuery();
            while (result.next()) {
                final PersistEntity rec = new PersistEntity();
                rec.setId(result.getLong(1));
                rec.setName(result.getString(2));
                final PersistEntityInner inner = new PersistEntityInner();
                inner.setId(result.getLong(3));
                rec.setInner(inner);
                clo.apply(rec.getId(), rec);
            }
            result.close();
        } catch (SQLException e) {
            throw new CacheLoaderException("Error loading cache.", e);
        }
    }

    @Override
    public PersistEntity load(Long aLong) throws CacheLoaderException {
        System.out.println("Load invoked");
        //All record must be loaded at load cache
        return null;
    }

    @Override
    public Map<Long, PersistEntity> loadAll(Iterable<? extends Long> iterable) throws CacheLoaderException {
        System.out.println("Load all invoked");
        //All record must be loaded at load cache
        return null;
    }

    @Override
    public void write(Cache.Entry<? extends Long, ? extends PersistEntity> entry) throws CacheWriterException {
        System.out.println("Write invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement insert = conn.prepareStatement("INSERT INTO persist_entity(id, name, persist_entity_inner_id) VALUES (?, ?, ?)");
            final PreparedStatement update = conn.prepareStatement("UPDATE persist_entity SET name = ?, persist_entity_inner_id = ? WHERE id = ?");
            final PersistEntity rec = entry.getValue();
            update.setString(1, rec.getName());
            update.setLong(2, rec.getInner().getId());
            update.setLong(3, rec.getId());
            if (update.executeUpdate() == 0) {
                insert.setLong(1, rec.getId());
                insert.setString(2, rec.getName());
                insert.setLong(3, rec.getInner().getId());
                insert.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error persist entity.", e);
        }
    }

    @Override
    public void writeAll(Collection<Cache.Entry<? extends Long, ? extends PersistEntity>> collection) throws CacheWriterException {
        System.out.println("Write all invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement insert = conn.prepareStatement("INSERT INTO persist_entity(id, name, persist_entity_inner_id) VALUES (?, ?, ?)");
            final PreparedStatement update = conn.prepareStatement("UPDATE persist_entity SET name = ?, persist_entity_inner_id = ? WHERE id = ?");
            for (Cache.Entry<? extends Long, ? extends PersistEntity> entry : collection) {
                final PersistEntity rec = entry.getValue();
                update.setString(1, rec.getName());
                update.setLong(2, rec.getInner().getId());
                update.setLong(3, rec.getId());
                if (update.executeUpdate() == 0) {
                    insert.setLong(1, rec.getId());
                    insert.setString(2, rec.getName());
                    insert.setLong(3, rec.getInner().getId());
                    insert.addBatch();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error persist entity.", e);
        }
    }

    @Override
    public void delete(Object o) throws CacheWriterException {
        System.out.println("Delete invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement delete = conn.prepareStatement("DELETE FROM persist_entity WHERE id = ?");
            final PreparedStatement deleteInner = conn.prepareStatement("DELETE FROM persist_entity_inner WHERE id = ?");
            final PersistEntity rec = getStorage().get((Long)o, PersistEntity.class);
            delete.setLong(1, (Long)o);
            delete.executeUpdate();
            if (rec.getInner() != null) {
                deleteInner.setLong(1, rec.getInner().getId());
                deleteInner.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error deleting entry.", e);
        }
    }

    @Override
    public void deleteAll(Collection<?> collection) throws CacheWriterException {
        System.out.println("Delete all invoked");
        try {
            final Connection conn = getConnection();
            final PreparedStatement delete = conn.prepareStatement("DELETE FROM persist_entity WHERE id = ?");
            final PreparedStatement deleteInner = conn.prepareStatement("DELETE FROM persist_entity_inner WHERE id = ?");
            for (Object o : collection) {
                final PersistEntity rec = getStorage().get((Long)o, PersistEntity.class);
                delete.setLong(1, (Long) o);
                delete.addBatch();
                if (rec.getInner() != null) {
                    deleteInner.setLong(1, rec.getInner().getId());
                    deleteInner.addBatch();
                }
            }
            delete.executeBatch();
            deleteInner.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new CacheWriterException("Error deleting entry.", e);
        }
    }
}
