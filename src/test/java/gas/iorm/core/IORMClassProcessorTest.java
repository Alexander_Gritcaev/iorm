package gas.iorm.core;

import gas.iorm.core.api.service.IORMClassProcessor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alex on 14.06.16.
 */
public class IORMClassProcessorTest {

    @Test
    public void test() throws Exception {
        IORMClassProcessor proc = new IORMClassProcessor();
        proc.processClasses("gas.iorm.core.CacheEntity");
//        Field ff = CacheEntity.class.getDeclaredField("___factory");
//        final IORMFactory factory = new IORMFactory(CacheEntity.class);
//        ff.set(null, factory);
        CacheEntity ent = new CacheEntity();

        Field fld = CacheEntity.class.getDeclaredField("createdBy");
        QuerySqlField[] ann = fld.getAnnotationsByType(QuerySqlField.class);
        ent.setId(9);
        User user = new User();
        user.setId(10);
        List<User> users = new LinkedList<User>();
        users.add(user);
        ent.setUserList(users);
        ent = null;
    }
}
