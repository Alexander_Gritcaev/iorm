package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * Created by alex on 16.06.16.
 */
public class Parent implements IORMPersistable {
    @QuerySqlField(index = true)
    private long id;
    private String parentName;
    private User parentUser;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public User getParentUser() {
        return parentUser;
    }

    public void setParentUser(User parentUser) {
        this.parentUser = parentUser;
    }
}
