package gas.iorm.core;

import javax.cache.configuration.Factory;

/**
 * Created by alex on 15.07.16.
 */
public class InnerStorageFactory implements Factory<InnerPersistenceAdapter> {

    public InnerPersistenceAdapter create() {
        return new InnerPersistenceAdapter();
    }
}
