package gas.iorm.core;

import gas.iorm.core.entity.PersistEntity;

import javax.cache.configuration.Factory;

/**
 * Created by alex on 27.07.16.
 */
public class StorageFactory implements Factory<PersistenceAdapter> {
    @Override
    public PersistenceAdapter create() {
        return new PersistenceAdapter();
    }
}
