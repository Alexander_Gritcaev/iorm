package gas.iorm.core;

import gas.iorm.core.api.criteria.IORMSort;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.transactions.Transaction;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by alex on 10.06.16.
 */
public class IORMIteratorTest {

    @Test
    public void test() throws Exception {
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setClassNames("gas.iorm.core.User", "gas.iorm.core.CacheEntity");
        final IORMFactory factory = IORMFactory.getInstance(config);

        IORMStorage storage = factory.getStorage();


        Transaction tx = storage.startTransaction();
        for (long l = 0; l < 1000; l++) {
            final CacheEntity ent = storage.create(CacheEntity.class);
            ent.setLabel("Строка ИД " + Long.toString(ent.getId()));
            ent.setTrans("Трансиент поле");
            for (int i = 0; i < 6; i++) {
                final User user = storage.create(User.class);
                user.setName("Пользователь " + l + " -- " + i);
                user.setPass("Пароль пользователя " + l + " - " + i);
                ent.getUserList().add(user);
                storage.add(user);
            }
            User creationUser = storage.create(User.class);
            creationUser.setName("Создавший пользователь для " + l);
            ent.setCreatedBy(creationUser);
            storage.add(creationUser);
            storage.add(ent);
        }
        tx.commit();

        List<CacheEntity> sql = storage.executeQuery(CacheEntity.class, "from CacheEntity, \"gas.iorm.core.User\".User where CacheEntity.createdBy = User.id AND User.name LIKE ?",
                "Создавший пользователь для 5%");
        System.out.println("Sql size is " + sql.size());
        assert(sql.size() == 111);

        CacheEntity rec = sql.get(0);
        final Iterator<User> iter = rec.getUserList().iterator();
        for (int i = 0; i < 2; i++) {
            iter.next();
            iter.remove();
        }

        Transaction tr = storage.startTransaction();
        storage.add(rec);
        tr.commit();

        List<CacheEntity> sql2 = storage.executeQuery(CacheEntity.class, "from CacheEntity, \"gas.iorm.core.User\".User where CacheEntity.createdBy = User.id AND User.name LIKE ?",
                "Создавший пользователь для 5%");
        assert sql2.get(0).getUserList().size() == 4;


        rec = sql2.get(0);
        ListIterator<User> listIter = rec.getUserList().listIterator();
        for (int i = 0; i < 2; i++) {
            listIter.next();
            listIter.remove();
        }
        User u = storage.create(User.class);
        u.setName("New name");
        u.setPass("New pass");
        storage.add(u);
        listIter.add(u);
        Transaction tt = storage.startTransaction();
        storage.add(rec);
        tt.commit();


        List<CacheEntity> sql3 = storage.createQuery(CacheEntity.class)
                .where().field("createdBy.name", String.class).likep("name")
                .orderBy().order("id", IORMSort.Order.ASC)
                .toQuery()
                .setParameter("name", "Создавший пользователь для 5%")
                .execute();
        assert sql3.get(0).getUserList().size() == 3;



        factory.stop();
    }
}
