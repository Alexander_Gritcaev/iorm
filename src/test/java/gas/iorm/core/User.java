package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPersistable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * Created by alex on 14.06.16.
 */
public class User implements IORMPersistable {
    @QuerySqlField
    private long id;
    @QuerySqlField(index = true)
    private String name;
    private String pass;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
