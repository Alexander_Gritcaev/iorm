package gas.iorm.core;

import gas.iorm.core.api.entity.IORMPersistable;

/**
 * Created by alex on 16.06.16.
 */
public class SecondEntity extends Parent {
    private String secondName;

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
