package gas.iorm.core;

import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.cache.store.CacheStoreSession;
import org.apache.ignite.resources.CacheStoreSessionResource;

import javax.cache.integration.CacheWriterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by alex on 27.07.16.
 */
public class BaseAdapter {

    @CacheStoreSessionResource
    protected CacheStoreSession session;

    private IORMStorage storage;
    private Lock storageLock = new ReentrantLock();

    protected IORMStorage getStorage() {
        if (storage == null) {
            try {
                if (storageLock.tryLock(10, TimeUnit.SECONDS) && storage == null) {
                    storage = IORMFactory.getInstance().getStorage();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return storage;
    }

    public void sessionEnd(boolean commit) throws CacheWriterException {
        System.out.println("Session end invoked");
        if (commit && session.isWithinTransaction()) {
            Connection conn = null;
            try {
                conn = getConnection();
                conn.commit();
            } catch (SQLException e) {
                try {
                    if (conn != null) {
                        conn.rollback();
                        conn.close();
                    }
                } catch (SQLException ee) {/*NONE*/}
                throw new CacheWriterException("Error commit data", e);
            }
        }
    }

    protected Connection getConnection() throws SQLException {
        Connection conn = session.attachment();
        if (conn == null || conn.isClosed()) {
            conn = openConnection(false);
            session.attach(conn);
        }
        if (session.isWithinTransaction()) {
            conn.setAutoCommit(false);
        } else {
            conn.setAutoCommit(true);
        }
        return conn;
    }

    private Connection openConnection(boolean autoCommit) throws SQLException {
        String url = "jdbc:postgresql://localhost/iormdb?user=postgres&password=";
        final Connection conn = DriverManager.getConnection(url);
        conn.setAutoCommit(autoCommit);
        return conn;
    }
}
