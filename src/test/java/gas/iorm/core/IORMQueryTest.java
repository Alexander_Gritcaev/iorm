package gas.iorm.core;

import gas.iorm.core.api.criteria.IORMExpression;
import gas.iorm.core.api.criteria.IORMQuery;
import gas.iorm.core.api.criteria.IORMQueryBuilder;
import gas.iorm.core.api.criteria.IORMSort;
import gas.iorm.core.api.criteria.impl.IORMQueryBuilderImpl;
import gas.iorm.core.entity.NotInFactory;
import gas.iorm.core.entity.QueryEntity;
import gas.iorm.core.entity.QueryEntityInner;
import gas.iorm.core.entity.QuerySecond;
import gas.iorm.core.storage.IORMStorage;
import org.apache.ignite.cache.CacheMetrics;
import org.apache.ignite.cluster.ClusterMetrics;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.transactions.Transaction;
import org.apache.ignite.transactions.TransactionConcurrency;
import org.apache.ignite.transactions.TransactionIsolation;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.sql.SQLData;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by alex on 01.07.16.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IORMQueryTest extends Assert {

    private static IORMFactory factory;
    private static IORMStorage storage;

    private final Logger logger = Logger.getLogger("Query Test");

    @BeforeClass
    public static void before() throws Exception {
        final IORMFactoryConfiguration config = new IORMFactoryConfiguration();
        config.setClassNames("gas.iorm.core.entity.QueryEntity", "gas.iorm.core.entity.QuerySecond",
                "gas.iorm.core.entity.QueryEntityInner");
        factory = IORMFactory.getInstance(config);
        factory = IORMFactory.getInstance(config);

        printClusterMetrics();
        System.gc();
        final long startMemory = factory.getNodeCollection().iterator().next().metrics().getHeapMemoryUsed();

        storage = factory.getStorage();

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(2016-1900, 3, 1));
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        final Transaction transaction = storage.startTransaction(TransactionConcurrency.PESSIMISTIC, TransactionIsolation.SERIALIZABLE);
        final List<QueryEntity> listEntity = new LinkedList<QueryEntity>();
        final List<QueryEntityInner> listInner = new LinkedList<QueryEntityInner>();
        final List<QuerySecond> listSecond = new LinkedList<QuerySecond>();
        long point1 = System.currentTimeMillis();

        for (int i = 0; i < 10; i++) {
            final QueryEntity e = storage.create(QueryEntity.class);
            e.setAmount(Math.random() * 1000000.0);
            e.setStartDate(calendar.getTime());
            calendar.add(Calendar.HOUR, 2);
            e.setEndDate(calendar.getTime());
            final QueryEntityInner inner = storage.create(QueryEntityInner.class);
            inner.setName("Inner name and random " + Math.ceil(Math.random() * 10000));

            e.setParentInner(inner);

            final QuerySecond second = storage.create(QuerySecond.class);
            second.setSecondName("Second name and random " + Math.ceil(Math.random() * 10000));
            inner.setSecond(second);
            storage.add(second);
            storage.add(inner);
            storage.add(e);
        }
        long point2 = System.currentTimeMillis();

        System.out.println("Records inserted in " + (point2 - point1) + " mills\n");
        printClusterMetrics();

        System.gc();
        final long finalMemory = factory.getNodeCollection().iterator().next().metrics().getHeapMemoryUsed();
        System.out.println("Total used memory: " + (finalMemory - startMemory)/ (1024L * 1024L) + " Mb.\n");

        transaction.commit();
    }

    @Test
    public void test01() throws Exception {

        Calendar c = Calendar.getInstance();
        c.set(2016, 7, 20);
        final Date startDate = c.getTime();
        c.set(2016, 7, 30);
        final Date endDate = c.getTime();
        final double val1 = 100;
        final double val2 = 500;

        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if ((rec.getAmount() > val1 && rec.getAmount() < val2) &&
                    (rec.getStartDate().after(startDate) && rec.getEndDate().before(endDate))) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 1 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        long point3 = System.currentTimeMillis();
        final IORMQueryBuilder<QueryEntity> qb = storage.createQuery(QueryEntity.class);
        final IORMExpression<QueryEntity> dateComplex = qb.complex();
        dateComplex.field("startDate", Date.class).gtp("startDate").and().field("endDate", Date.class).ltp("endDate");
        final List<QueryEntity> resultQuery = qb.where().field("amount", double.class).gtp("amountFrom").and().field("amount", double.class)
                .ltp("amountTo").and().complex(dateComplex).toQuery()
                .setParameter("startDate", startDate).setParameter("endDate", endDate)
                .setParameter("amountTo", 500.0).setParameter("amountFrom", 100.0)
                .execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 1 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }

    @Test
    public void test02() throws Exception {

        Calendar c = Calendar.getInstance();
        c.set(2016, 7, 20);
        final Date startDate = c.getTime();
        c.set(2016, 7, 30);
        final Date endDate = c.getTime();
        final double val1 = 100.0;
        final double val2 = 500.0;

        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if (rec.getAmount() > val1 && rec.getAmount() < val2) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 2 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> resultQuery = storage.createQuery(QueryEntity.class).where().field("amount", double.class)
                .gtp("amountFrom").and().field("amount", double.class).ltp("amountTo").toQuery()
                .setParameter("amountFrom", val1).setParameter("amountTo", val2).execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 2 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }

    @Test
    public void test03() throws Exception {
        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if (rec.getParentInner().getSecond().getSecondName().contains("Second name and random 50")) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 3 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> resultQuery = storage.createQuery(QueryEntity.class).where()
                .field("parentInner.second.secondName", String.class).likep("secondName").toQuery()
                .setParameter("secondName", "Second name and random 50%").execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 3 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }


    @Test
    public void test04() throws Exception {
        final List<Long> vals = new LinkedList<Long>();
        for (long l = (Long.MIN_VALUE + 1); l < (Long.MIN_VALUE + 20); l++) {
            vals.add(l);
        }

        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if (vals.contains(rec.getId())) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 4 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        long point3 = System.currentTimeMillis();
        final IORMQueryBuilderImpl<QueryEntity> qb = storage.createQuery(QueryEntity.class);
        final IORMQuery<QueryEntity> query = qb.where().field("id", long.class).inp("idList").toQuery();
        final List<QueryEntity> resultQuery = query.setParameter("idList", (Object)vals.toArray(new Long[vals.size()])).execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 4 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }

    @Test
    public void test05() throws Exception {
        final List<Long> vals = new LinkedList<Long>();
        for (long l = (Long.MIN_VALUE + 1); l < (Long.MIN_VALUE + 20); l++) {
            vals.add(l);
        }

        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if (vals.contains(rec.getParentInner().getId()) && rec.getAmount() > 1000.0 && rec.getAmount() < 500000.0) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 5 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        long point3 = System.currentTimeMillis();
        final IORMQueryBuilderImpl<QueryEntity> qb = storage.createQuery(QueryEntity.class);
        final IORMQuery<QueryEntity> query = qb.where().field("parentInner.id", long.class).inp("innerIds")
                .and().field("amount", double.class).gt(1000.0).and().field("amount", double.class).lt(500000.0).toQuery();
        final List<QueryEntity> resultQuery = query.setParameter("innerIds", vals.toArray(new Long[vals.size()])).execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 5 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }

    @Test
    public void test06() throws Exception {
        final List<Long> vals = new LinkedList<Long>();
        for (long l = (Long.MIN_VALUE + 1); l < (Long.MIN_VALUE + 20); l++) {
            vals.add(l);
        }

        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        final List<QueryEntity> resultFor = new LinkedList<QueryEntity>();
        for (QueryEntity rec : all) {
            if (vals.contains(rec.getParentInner().getId()) && vals.contains(rec.getId()) && rec.getAmount() > 1000.0 && rec.getAmount() < 500000.0) {
                resultFor.add(rec);
            }
        }
        long point2 = System.currentTimeMillis();
        System.out.println("Test 6 For complete in " + (point2 - point1) + " mills. Count " + resultFor.size());
        all.clear();
        resultFor.clear();

        final Object[] v = vals.toArray(new Long[vals.size()]);
        long point3 = System.currentTimeMillis();
        final IORMQueryBuilderImpl<QueryEntity> qb = storage.createQuery(QueryEntity.class);
        final IORMQuery<QueryEntity> query = qb.where().field("parentInner.id", long.class).inp("innerIds")
                .and().field("id", long.class).inp("ids")
                .and().field("amount", double.class).gt(1000.0).and().field("amount", double.class).lt(500000.0).toQuery();
        final List<QueryEntity> resultQuery = query.setParameter("innerIds", v)
                .setParameter("ids", v).execute();
        long point4 = System.currentTimeMillis();
        System.out.println("Test 5 Query complete in " + (point4 - point3) + " mills. Count " + resultQuery.size());

        assert(resultFor.size() == resultQuery.size());
    }

    @Test
    public void test07() throws Exception {
        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        Collections.sort(all, new Comparator<QueryEntity>() {
            public int compare(QueryEntity o1, QueryEntity o2) {
                return (int)(o2.getId() - o1.getId());
            }
        });
        QueryEntity forRes = all.get(0);
        long point2 = System.currentTimeMillis();
        System.out.println("Test 7 Sort complete in " + (point2 - point1) + " id is " + forRes.getId());

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> sorted = storage.createQuery(QueryEntity.class).orderBy().order("id", IORMSort.Order.DESC).toQuery().execute();
        QueryEntity sortRes = sorted.get(0);
        long point4 = System.currentTimeMillis();
        System.out.println("Test 7 OrderBy complete in " + (point4 - point3) + " id is " + sortRes.getId());
        assertTrue(forRes.getId() == sortRes.getId());
    }

    @Test
    public void test08() throws Exception {
        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        Collections.sort(all, new Comparator<QueryEntity>() {
            public int compare(QueryEntity o1, QueryEntity o2) {
                return (int)(o2.getParentInner().getId() - o1.getParentInner().getId());
            }
        });
        QueryEntity forRes = all.get(0);
        long point2 = System.currentTimeMillis();
        System.out.println("Test 8 Sort complete in " + (point2 - point1) + " id is " + forRes.getId());

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> sorted = storage.createQuery(QueryEntity.class).orderBy().order("parentInner.id", IORMSort.Order.DESC).toQuery().execute();
        QueryEntity sortRes = sorted.get(0);
        long point4 = System.currentTimeMillis();
        System.out.println("Test 8 OrderBy complete in " + (point4 - point3) + " id is " + sortRes.getId());
        assertTrue(forRes.getId() == sortRes.getId());
    }

    @Test
    public void test09() throws Exception {
        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        Collections.sort(all, new Comparator<QueryEntity>() {
            public int compare(QueryEntity o1, QueryEntity o2) {
                return (int)(o2.getParentInner().getSecond().getId() - o1.getParentInner().getSecond().getId());
            }
        });
        QueryEntity forRes = all.get(0);
        long point2 = System.currentTimeMillis();
        System.out.println("Test 8 Sort complete in " + (point2 - point1) + " id is " + forRes.getId());

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> sorted = storage.createQuery(QueryEntity.class).orderBy().order("parentInner.second.id", IORMSort.Order.DESC).toQuery().execute();
        QueryEntity sortRes = sorted.get(0);
        long point4 = System.currentTimeMillis();
        System.out.println("Test 8 OrderBy complete in " + (point4 - point3) + " id is " + sortRes.getId());
        assertTrue(forRes.getId() == sortRes.getId());
    }

    @Test
    public void test10() throws Exception {
        long point1 = System.currentTimeMillis();
        final List<QueryEntity> all = new LinkedList<QueryEntity>(storage.getAll(QueryEntity.class));
        Collections.sort(all, new Comparator<QueryEntity>() {
            public int compare(QueryEntity o1, QueryEntity o2) {
                final int res = (int)(o2.getParentInner().getId() - o1.getParentInner().getId());
                if (res != 0) {
                    return res;
                } else {
                    return (int) (o2.getParentInner().getSecond().getId() - o1.getParentInner().getSecond().getId());
                }
            }
        });
        QueryEntity forRes = all.get(0);
        long point2 = System.currentTimeMillis();
        System.out.println("Test 8 Sort complete in " + (point2 - point1) + " id is " + forRes.getId());

        long point3 = System.currentTimeMillis();
        final List<QueryEntity> sorted = storage.createQuery(QueryEntity.class).orderBy()
                .order("parentInner.id", IORMSort.Order.DESC)
                .order("parentInner.second.id", IORMSort.Order.DESC).toQuery().execute();
        QueryEntity sortRes = sorted.get(0);
        long point4 = System.currentTimeMillis();
        System.out.println("Test 8 OrderBy complete in " + (point4 - point3) + " id is " + sortRes.getId());
        assertTrue(forRes.getId() == sortRes.getId());
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    @Test
    public void test11() throws Exception {
        expectedEx.expect(RuntimeException.class);
        storage.createQuery(NotInFactory.class);
    }

    private static void printClusterMetrics() {
        for (ClusterNode node : factory.getNodeCollection()) {
            final ClusterMetrics metrics = node.metrics();
            System.out.println("Node " + node.id().toString() + " committed Heap memory " + metrics.getHeapMemoryCommitted() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " used Heap memory " + metrics.getHeapMemoryUsed() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " initialized Heap memory " + metrics.getHeapMemoryInitialized() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " total Heap memory " + metrics.getHeapMemoryTotal() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " max Heap memory " + metrics.getHeapMemoryMaximum() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " committed NON Heap memory " + metrics.getNonHeapMemoryCommitted() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " used NON Heap memory " + metrics.getNonHeapMemoryUsed() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " initialized NON Heap memory " + metrics.getNonHeapMemoryInitialized() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " total NON Heap memory " + metrics.getNonHeapMemoryTotal() / (1024L * 1024L) + " Mb." );
            System.out.println("Node " + node.id().toString() + " max NON Heap memory " + metrics.getNonHeapMemoryMaximum() / (1024L * 1024L) + " Mb.\n" );


//            if (storage != null) {
//                final CacheMetrics cacheMetrics = storage.getMetrics(QueryEntity.class, factory.groupNodes(node));
//                printCacheMetrics(cacheMetrics);
//                printCacheMetrics(storage.getMetrics(QueryEntityInner.class));
//                printCacheMetrics(storage.getMetrics(QuerySecond.class));
//            }
        }
    }

    private static void printCacheMetrics(CacheMetrics metrics) {
        System.out.println("Cache " + metrics.name() + " allocated NON Heap memory " + metrics.getOffHeapAllocatedSize() / (1024L * 1024L) + " Mb.\n" );
    }
}

